package fr.efrei.paumier.shared.selection;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FakeSelector implements Selector {

	private Map<SelectionContext, SelectorQueue> queue;

	public FakeSelector() {
		queue = Arrays.stream(SelectionContext.values())
				.collect(Collectors.toMap(Function.identity(), SelectorQueue::new));
	}

	/**
	 * Add multiple time provided rank to the queue of indexes to follow for the provided context
	 * @param context context in which those ranks will be considered
	 * @param rank rank to choose which item to select from the choices at the time. Negative means counted from the end.
	 * @param times amount of time this rank will be used
	 */
	public void enqueueRankMultipleTimes(SelectionContext context, int rank, int times) {
		for (int i = 0; i < times; i++)
			enqueueRanks(context, rank);
	}

	/**
	 * Add provided ranks to the queue of indexes to follow for the provided context
	 * @param context context in which those ranks will be considered
	 * @param ranks ranks to choose which item to select from the choices at the time. Negative means counted from the end.
	 */
	public void enqueueRanks(SelectionContext context, int... ranks) {
		queue.get(context).enqueueRanks(ranks);
	}

	@Override
	public <TItem> TItem selectAmong(List<TItem> choices, SelectionContext context) {
		return queue.get(context).selectAmong(choices);
	}

	public Map<SelectionContext, Integer> getRemainingQueues() {
		return queue.values()
				.stream()
				.filter(SelectorQueue::isNotEmpty)
				.collect(Collectors.toMap(SelectorQueue::getContext, SelectorQueue::size));
	}

	private class SelectorQueue {
		private final SelectionContext context;
		private final LinkedList<Integer> queue = new LinkedList<Integer>();

		public SelectorQueue(SelectionContext context) {
			this.context = context;
		}

		public void enqueueRanks(int... ranks) {
			for (int rank : ranks) {
				queue.offer(rank);
			}
		}

		public <TItem> TItem selectAmong(List<TItem> choices) {
			if (choices.size() == 0)
				return null;

			if (queue.isEmpty())
				throw new IllegalStateException("Queue " + context + " is empty");

			int index = queue.poll();
			if (index < 0)
				index += choices.size();

			return choices.get(index);
		}

		public SelectionContext getContext() {
			return context;
		}

		public boolean isNotEmpty() {
			return !queue.isEmpty();
		}

		public int size() {
			return queue.size();
		}
	}
}
