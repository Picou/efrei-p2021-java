package fr.efrei.paumier.shared.networking;

import java.io.Closeable;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class FakeMigrationServer implements Runnable, Closeable {
    private final ServerSocket serverSocket;

    private Socket incomingConnection;
    private ObjectOutputStream objectOutputStream;
    private ObjectInputStream objectInputStream;

    private final Thread connectionReceivingThread;

    public FakeMigrationServer() throws IOException {
        this.serverSocket = new ServerSocket(0); // 0 means random port

        this.connectionReceivingThread = new Thread(this);
        this.connectionReceivingThread.start();
    }

    @Override
    public void run() {
        try {
            accept();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private void accept() throws IOException {
        this.incomingConnection = this.serverSocket.accept();

        this.objectInputStream = new ObjectInputStream(incomingConnection.getInputStream());
        this.objectOutputStream = new ObjectOutputStream(incomingConnection.getOutputStream());
    }

    public void waitForAccept() throws InterruptedException {
        this.connectionReceivingThread.join();
    }

    public void close() {
        try {
            // no need to interrupt, this will make this.serverSocket.accept throw
            this.serverSocket.close();
            this.incomingConnection.close();
        } catch (IOException e) {
            // Socket was already closed, no need to do anything further
        }
    }

    public HelloMessage readHello() throws ClassNotFoundException, IOException {
        return (HelloMessage) objectInputStream.readObject();
    }

    public EmigrationMessage readMigration() throws ClassNotFoundException, IOException {
        return (EmigrationMessage) objectInputStream.readObject();
    }

    public void sendMessage(ImmigrationMessage message) throws IOException {
        this.objectOutputStream.writeObject(message);
    }

    public InetSocketAddress getAddress() {
        return (InetSocketAddress)this.serverSocket.getLocalSocketAddress();
    }
}
