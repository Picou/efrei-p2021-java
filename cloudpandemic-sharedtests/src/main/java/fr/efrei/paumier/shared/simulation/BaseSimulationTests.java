package fr.efrei.paumier.shared.simulation;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.efrei.paumier.shared.categories.*;
import fr.efrei.paumier.shared.networking.EmigrationMessage;
import fr.efrei.paumier.shared.networking.FakeMigrationServer;
import fr.efrei.paumier.shared.networking.HelloMessage;
import fr.efrei.paumier.shared.networking.ImmigrationMessage;
import fr.efrei.paumier.shared.orders.OrderType;
import fr.efrei.paumier.shared.selection.SelectionContext;
import fr.efrei.paumier.shared.traits.AgeCategory;
import fr.efrei.paumier.shared.traits.BasicTraitsProvider;
import fr.efrei.paumier.shared.traits.Interests;
import fr.efrei.paumier.shared.traits.TraitsProvider;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.efrei.paumier.shared.events.Event;
import fr.efrei.paumier.shared.selection.FakeSelector;
import fr.efrei.paumier.shared.selection.Selector;
import fr.efrei.paumier.shared.time.FakeClock;
import org.junit.experimental.categories.Category;

public abstract class BaseSimulationTests {

	private FakeSelector selector;
	private FakeClock clock;
	private Simulation simulation;
	
	@Before
	public void setUp() {
		clock = new FakeClock();
		selector = new FakeSelector();
		simulation = createSimulation(clock, selector, new BasicTraitsProvider(),100);
	}

	@After
	public void tearDown() {
		try {
			simulation.close();
		} catch (IOException e) {
			// We do not want to count this as an additional test failure
			e.printStackTrace();
		}
	}

	public void checkSelectorEmptiness() {
		Map<SelectionContext, Integer> remainingQueue = selector.getRemainingQueues();
		String errorMessage = "Selector has not been called as expected. Missing calls per context: " + remainingQueue;
		assertEquals(errorMessage, 0, remainingQueue.size());
	}
	
	protected List<Event> eventTriggered = new ArrayList<Event>();

	private Simulation createSimulation(Clock clock, Selector selector, TraitsProvider traitsProvider, int population) {
		return createSimulation(clock, selector, traitsProvider,100, 0, null);
	}

	protected abstract Simulation createSimulation(
			Clock clock, Selector selector, TraitsProvider traitsProvider,
			int population, int initialPanic,
			InetSocketAddress serverAddress);

	@Test
	@Category(Version1.class)
	public void starts_everybodyIsHealthy() {
		// Purpose : to test the original state

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(100, simulation.getLivingPopulation());
		assertEquals(0, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(0, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version1.class)
	public void sec003_onePersonIsInfected() {
		// Purpose : to test the timing and effect of initial infection

		/* Expected events :
		 * sec 00 : 4 screenings
		 * sec 01 : 5 screenings
		 * sec 02 : 5 screenings
		 * sec 03 : 1 initial infection, 1 screening
		 */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 2 + 1);
		selector.enqueueRanks(SelectionContext.InitialInfection, 0);

		clock.advance(Duration.ofSeconds(3));
		simulation.update();
		
		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(100, simulation.getLivingPopulation());
		assertEquals(1, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(0, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version1.class)
	public void sec008_twoPersonsAreInfected() {
		// Purpose : to test the timing and effect of spreading

		/* Expected events :
		 * sec 00 : 4 screenings
		 * sec 01 : 5 screenings
		 * sec 02 : 5 screenings
		 * sec 03 : 1 initial infection, 5 screenings
		 * sec 04 : 5 screenings
		 * sec 05 : 5 screenings
		 * sec 06 : 5 screenings
		 * sec 07 : 5 screenings
		 * sec 08 : 1 spread, 1 screenings
		 */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 7 + 1);
		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRanks(SelectionContext.Spreading, 1);

		clock.advance(Duration.ofSeconds(8));
		simulation.update();
		
		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(100, simulation.getLivingPopulation());
		assertEquals(2, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(0, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version1.class)
	public void sec013_fourPersonsAreInfected() {
		// Purpose : to confirm that second-hand infected spread the infection...
		// and also that first-hand infected continue to spread over time, not just one time

		/* Expected events :
		 * sec 00 : 4 screenings
		 * sec 01 : 5 screenings
		 * sec 02 : 5 screenings
		 * sec 03 : 1 initial infection, 5 screenings
		 * sec 04 : 5 screenings
		 * sec 05 : 5 screenings
		 * sec 06 : 5 screenings
		 * sec 07 : 5 screenings
		 * sec 08 : 1 spread, 5 screenings
		 * sec 09 : 5 screenings
		 * sec 10 : 5 screenings
		 * sec 11 : 5 screenings
		 * sec 12 : 5 screenings
		 * sec 13 : 2 spreads, 1 screening
		 */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 12 + 1);
		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRanks(SelectionContext.Spreading, 1); // sec 08
		selector.enqueueRanks(SelectionContext.Spreading, 2, 4); // sec 13

		clock.advance(Duration.ofSeconds(13));
		simulation.update();
		
		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(100, simulation.getLivingPopulation());
		assertEquals(4, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(0, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version2.class)
	public void sec018_firstDeath() {
		// Purpose : to confirm that second-hand infected spread over time the infection...
		// to test the timing and effect of death
		// to confirm the deceasing is resolved prior to spreading a third time
		// (hence infected spread 2 times in their lifetime, not 3 times)

		/* Expected events :
		 * sec 00 : 4 screenings
		 * sec 01 : 5 screenings
		 * sec 02 : 5 screenings
		 * sec 03 : 1 initial infection, 5 screenings
		 * sec 04 : 5 screenings
		 * sec 05 : 5 screenings
		 * sec 06 : 5 screenings
		 * sec 07 : 5 screenings
		 * sec 08 : 1 spread, 5 screenings
		 * sec 09 : 5 screenings
		 * sec 10 : 5 screenings
		 * sec 11 : 5 screenings
		 * sec 12 : 5 screenings
		 * sec 13 : 2 spreads, 5 screening
		 * sec 14 : 5 screenings
		 * sec 15 : 5 screenings
		 * sec 16 : 5 screenings
		 * sec 17 : 5 screenings
		 * sec 18 : 3 spreads, 1 screening
		 */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 17 + 1); // sec 00-18

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRanks(SelectionContext.Spreading, 1); // sec 08
		selector.enqueueRanks(SelectionContext.Spreading, 2, 4); // sec 13
		selector.enqueueRanks(SelectionContext.Spreading, 5, 6, 7); // sec 18

		clock.advance(Duration.ofSeconds(18));
		simulation.update();
		
		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(99, simulation.getLivingPopulation());
		assertEquals(6, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(1, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version2.class)
	public void sec018_deadAreNotScreened() {
		/* Expected events : same as sec018_firstDeath */

		// Purpose : to confirm that deceased inhabitants are not sent within the list of people to screen

		// Difference with the previous test is the two following lines
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, 3, 4 + 5 * 17); // sec 00-17
		selector.enqueueRanks(SelectionContext.Screening, 2); // sec 18 - second soldier *alive*, ie. soldier #3

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRanks(SelectionContext.Spreading, 1); // sec 08
		selector.enqueueRanks(SelectionContext.Spreading, 2, 4); // sec 13
		selector.enqueueRanks(SelectionContext.Spreading, 5, 6, 7); // sec 18

		clock.advance(Duration.ofSeconds(18));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(99, simulation.getLivingPopulation());
		assertEquals(6, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(1, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(EdgeCase.class)
	public void sec038_exponentialInfections () {
		// Purpose : to confirm the perpetual rhythm of spreading
		// Useful to confirm the base of the situation for longer tests (ie. far money tests)

		/* Expected events :
		 * Screenings as usual, never successful
		 * sec 03 : 1 initial infection (1/100 infected)
		 * sec 08 : 1 spread (2/100 infected)
		 * sec 13 : 2 spreads (4/100 infected)
		 * sec 18 : 1 death, 3 spreads (6/99 infected)
		 * sec 23 : 1 death, 5 spreads (10/98 infected)
		 * sec 28 : 2 deaths, 8 spreads (16/96 infected)
		 * sec 33 : 3 deaths, 13 spreads (26/93 infected)
		 * sec 38 : 5 deaths, 21 spreads (42/88 infected)
		 */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 37 + 1); // sec 00-38

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 5); // sec 23
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 8); // sec 28
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 13); // sec 33
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 21); // sec 38

		clock.advanceTo(Duration.ofMillis(23000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(98, simulation.getLivingPopulation());
		assertEquals(10, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(2, simulation.getDeadPopulation());

		clock.advanceTo(Duration.ofMillis(28000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(96, simulation.getLivingPopulation());
		assertEquals(16, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(4, simulation.getDeadPopulation());

		clock.advanceTo(Duration.ofMillis(33000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(93, simulation.getLivingPopulation());
		assertEquals(26, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(7, simulation.getDeadPopulation());

		clock.advanceTo(Duration.ofMillis(38000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(88, simulation.getLivingPopulation());
		assertEquals(42, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(12, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(EdgeCase.class)
	public void sec048_exponentialInfections_untilAllInfected () {
		// Purpose : to confirm the perpetual rhythm of spreading
		// Useful to confirm the base of the situation for longer tests (ie. far money tests)
		// Also useful to confirm the simulation does not crash when no more people can be infected

		/* Expected events :
		 * Screenings as usual the last of the queue, never successful, except the last one
		 * sec 03 : 1 initial infection (1/100 infected)
		 * sec 08 : 1 spread (2/100 infected)
		 * sec 13 : 2 spreads (4/100 infected)
		 * sec 18 : 1 death, 3 spreads (6/99 infected)
		 * sec 23 : 1 death, 5 spreads (10/98 infected)
		 * sec 28 : 2 deaths, 8 spreads (16/96 infected)
		 * sec 33 : 3 deaths, 13 spreads (26/93 infected)
		 * sec 38 : 5 deaths, 21 spreads (42/88 infected)
		 * sec 43 : 8 deaths, 34 spreads (68/80 infected)
		 * sec 48 : 13 deaths, 12 spreads (67/67 infected) - can't infect more
		 */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 47 + 1); // sec 00-48

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 5); // sec 23
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 8); // sec 28
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 13); // sec 33
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 21); // sec 38
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 34); // sec 43
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 12); // sec 48

		clock.advanceTo(Duration.ofMillis(43000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(80, simulation.getLivingPopulation());
		assertEquals(68, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(20, simulation.getDeadPopulation());

		clock.advanceTo(Duration.ofMillis(48000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(67, simulation.getLivingPopulation());
		assertEquals(67, simulation.getInfectedPopulation());
		assertEquals(1, simulation.getQuarantinedPopulation());
		assertEquals(33, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(EdgeCase.class)
	public void sec063_exponentialInfections_untilAllDead() {
		// Purpose : to confirm the perpetual rhythm of spreading, and dying
		// Useful to confirm the simulation runs at least until all inhabitant are dead
		// and also to confirm the simulation does not crash when there is no more inhabitant to screen

		/* Expected events :
		 * Screenings as usual the last of the queue, never successful, except the last one
		 * sec 03 : 1 initial infection (1/100 infected)
		 * sec 08 : 1 spread (2/100 infected)
		 * sec 13 : 2 spreads (4/100 infected)
		 * sec 18 : 1 death, 3 spreads (6/99 infected)
		 * sec 23 : 1 death, 5 spreads (10/98 infected)
		 * sec 28 : 2 deaths, 8 spreads (16/96 infected)
		 * sec 33 : 3 deaths, 13 spreads (26/93 infected)
		 * sec 38 : 5 deaths, 21 spreads (42/88 infected)
		 * sec 43 : 8 deaths, 34 spreads (68/80 infected)
		 * sec 48 : 13 deaths, 12 spreads (67/67 infected) - can't infect more
		 *
		 * Between sec48 and sec52.8, we start screening the first of the queue (not the last one)
		 * This puts them in quarantined. From the 25 screenings that occurs, the first 21 affect inhabitant too close
		 * to their deaths to be saved. The next 4 however affect an inhabitant further away from its death.
		 * They are quarantined at 52.2 (#54), 52.4 (#55), 52.6 (#56) and 52.8 (#57)
		 *
		 * sec 53 : 21 deaths, 5 spreads (46/46 infected - 5 quarantined)
		 *
		 * There are 29 inhabitants about to die (34 from sec43 - 4 that we're about to save)
		 * As long as we do not quarantine the last 13 inhabitants, whoever else we screen won't change the outcome:
		 * Only the last 4 we're about to cure will stay alive until the end.
		 * They will exit quarantine at 57.2, 57.4, 57.6 and 57.8.
		 *
		 * sec 58 : 29 deaths, 4 spreads (16/16 infected - 1 quarantined)
		 * The final fours are infected again, then the first is put into quarantine
		 * He'll be saved at 63.
		 *
		 * sec 63 : 12 deaths, 0 spreads (3/4 infected)
		 * #54 is cured, but all infected are in quarantine
		 *
		 * sec 63.2 : 0 deaths, 0 spreads (2/4 infected)
		 * sec 63.4 : 0 deaths, 0 spreads (1/4 infected)
		 * sec 63.6 : 0 deaths, 0 spreads (0/4 infected)
		 */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 47); // sec 00-47.8
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, 0, 25); // sec 48-52.8
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, 0, 25); // sec 53-57.8
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, 0, 20); // sec 58-end

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 5); // sec 23
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 8); // sec 28
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 13); // sec 33
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 21); // sec 38
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 34); // sec 43
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 12); // sec 48
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 4); // sec 53

		clock.advanceTo(Duration.ofMillis(53000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(46, simulation.getLivingPopulation());
		assertEquals(46, simulation.getInfectedPopulation());
		assertEquals(5, simulation.getQuarantinedPopulation());
		assertEquals(54, simulation.getDeadPopulation());

		clock.advanceTo(Duration.ofMillis(58000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(16, simulation.getLivingPopulation());
		assertEquals(16, simulation.getInfectedPopulation());
		assertEquals(1, simulation.getQuarantinedPopulation());
		assertEquals(84, simulation.getDeadPopulation());

		clock.advanceTo(Duration.ofMillis(63000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(4, simulation.getLivingPopulation());
		assertEquals(3, simulation.getInfectedPopulation());
		assertEquals(3, simulation.getQuarantinedPopulation());
		assertEquals(96, simulation.getDeadPopulation());

		clock.advanceTo(Duration.ofMillis(63200));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(4, simulation.getLivingPopulation());
		assertEquals(2, simulation.getInfectedPopulation());
		assertEquals(2, simulation.getQuarantinedPopulation());
		assertEquals(96, simulation.getDeadPopulation());

		clock.advanceTo(Duration.ofMillis(63400));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(4, simulation.getLivingPopulation());
		assertEquals(1, simulation.getInfectedPopulation());
		assertEquals(1, simulation.getQuarantinedPopulation());
		assertEquals(96, simulation.getDeadPopulation());

		clock.advanceTo(Duration.ofMillis(63600));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(4, simulation.getLivingPopulation());
		assertEquals(0, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(96, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version2.class)
	public void screening_putInQuarantine() {
		// Purpose : to test the effect of screening an infected
		// and to confirm screening are resolved after initial infection when occurring at the same time

		/* Expected events :
		 * sec 00 : 4 screenings
		 * sec 01 : 5 screenings
		 * sec 02 : 5 screenings
		 * sec 03 : 1 initial infection, 1 screening
		 */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 2); // sec 00-02
		selector.enqueueRanks(SelectionContext.Screening, 3); // sec 03

		selector.enqueueRanks(SelectionContext.InitialInfection, 3);

		clock.advance(Duration.ofSeconds(3));
		simulation.update();
		
		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(100, simulation.getLivingPopulation());
		assertEquals(1, simulation.getInfectedPopulation());
		assertEquals(1, simulation.getQuarantinedPopulation());
		assertEquals(0, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version2.class)
	public void screening_early_preventsDeath() {
		// Purpose : to test the timing and effect of healing a quarantined inhabitant

		/* Expected events :
		 * sec 00 : 4 screenings
		 * sec 01 : 5 screenings
		 * sec 02 : 5 screenings
		 * sec 03 : 1 initial infection, 5 screenings
		 * sec 04 : 5 screenings
		 * sec 05 : 5 screenings
		 * sec 06 : 5 screenings
		 * sec 07 : 5 screenings
		 * sec 08 : 1 screening
		 */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 2); // sec 00
		selector.enqueueRanks(SelectionContext.Screening, 3); // sec 03.000
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 4 + 1); // sec 03.200-08

		selector.enqueueRanks(SelectionContext.InitialInfection, 3);

		clock.advance(Duration.ofSeconds(8));
		simulation.update();
		
		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(100, simulation.getLivingPopulation());
		assertEquals(0, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(0, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version2.class)
	public void screening_justInTime_preventsDeath() {
		// Purpose : to test the timing and effect of healing a quarantined inhabitant
		// compared to the death lifetime (case where it's just in time to be successful)

		/* Expected events :
		 * sec 00 : 4 screenings
		 * sec 01 : 5 screenings
		 * sec 02 : 5 screenings
		 * sec 03 : 1 initial infection, 5 screenings
		 * sec 04 : 5 screenings
		 * sec 05 : 5 screenings
		 * sec 06 : 5 screenings
		 * sec 07 : 5 screenings
		 * sec 08 : 1 spread, 5 screenings
		 * sec 09 : 5 screenings
		 * sec 10 : 5 screenings
		 * sec 11 : 5 screenings
		 * sec 12 : 5 screenings
		 * sec 13 : 1 spread, 5 screening
		 * sec 14 : 5 screenings
		 * sec 15 : 5 screenings
		 * sec 16 : 5 screenings
		 * sec 17 : 5 screenings
		 * sec 18 : 2 spreads, 1 screening
		 */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 11 + 4); // sec 00-12.600
		selector.enqueueRanks(SelectionContext.Screening, 0); // sec 12.800
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 5 * 5 + 1); // sec 13-18

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRanks(SelectionContext.Spreading, 1); // sec 08
		selector.enqueueRanks(SelectionContext.Spreading, 2); // sec 13
		selector.enqueueRanks(SelectionContext.Spreading, 5, 6); // sec 18

		clock.advance(Duration.ofSeconds(18));
		simulation.update();
		
		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(100, simulation.getLivingPopulation());
		assertEquals(4, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(0, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version2.class)
	public void screening_late_doesNotPreventsDeath() {
		// Purpose : to test the timing and failure to heal a quarantined inhabitant
		// when it's "just" to late.
		// Which is to also say, dying is resolved prior to healing when occurring at the same time.

		/* Expected events :
		 * sec 00 : 4 screenings
		 * sec 01 : 5 screenings
		 * sec 02 : 5 screenings
		 * sec 03 : 1 initial infection, 5 screenings
		 * sec 04 : 5 screenings
		 * sec 05 : 5 screenings
		 * sec 06 : 5 screenings
		 * sec 07 : 5 screenings
		 * sec 08 : 1 spread, 5 screenings
		 * sec 09 : 5 screenings
		 * sec 10 : 5 screenings
		 * sec 11 : 5 screenings
		 * sec 12 : 5 screenings
		 * sec 13 : 2 spreads, 5 screening
		 * sec 14 : 5 screenings
		 * sec 15 : 5 screenings
		 * sec 16 : 5 screenings
		 * sec 17 : 5 screenings
		 * sec 18 : 3 spreads, 1 screening
		 */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 12); // sec 00-12
		selector.enqueueRanks(SelectionContext.Screening, 0); // sec 13.000
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 4 + 1); // sec 13.200-18

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRanks(SelectionContext.Spreading, 1); // sec 08
		selector.enqueueRanks(SelectionContext.Spreading, 2, 4); // sec 13
		selector.enqueueRanks(SelectionContext.Spreading, 5, 6, 7); // sec 18

		clock.advance(Duration.ofSeconds(18));
		simulation.update();
		
		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(99, simulation.getLivingPopulation());
		assertEquals(6, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(1, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version3.class)
	public void money_startsAtZero() {
		// Purpose : to test the initial status in regard to money
		simulation.update();

		assertEquals(0, simulation.getMoney());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version3.class)
	public void money_stillAtZero_atSec19dot9() {
		// Purpose : to confirm, taxes do not occur before 20 seconds

		/* Expected events : same as sec018_firstDeath, with 4 additional screenings
		* This means 1 deceased, 6 infected, and 0 quarantined
		* */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 19); // sec 00-19.8

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRanks(SelectionContext.Spreading, 1); // sec 08
		selector.enqueueRanks(SelectionContext.Spreading, 2, 4); // sec 13
		selector.enqueueRanks(SelectionContext.Spreading, 5, 6, 7); // sec 18

		clock.advance(Duration.ofMillis(19999));
		simulation.update();

		assertEquals(0, simulation.getMoney());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version3.class)
	public void money_increasedAtSec20_byInhabitants() {
		// Purpose : to confirm, taxes do occurs after 20 seconds precisely
		// and test that deceased do not pay, but living ones (including infected) pays 1

		/* Expected events : same as sec018_firstDeath, with 5 additional screenings
		 * This means 1 deceased, 6 infected, and 0 quarantined
		 * */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 19 + 1); // sec 00-19.8

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRanks(SelectionContext.Spreading, 1); // sec 08
		selector.enqueueRanks(SelectionContext.Spreading, 2, 4); // sec 13
		selector.enqueueRanks(SelectionContext.Spreading, 5, 6, 7); // sec 18

		clock.advance(Duration.ofMillis(20000));
		simulation.update();

		assertEquals(99, simulation.getMoney());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version3.class)
	public void money_increasedAtSec40_byInhabitants() {
		// Purpose : to confirm, taxes occurs *every* 20 seconds precisely
		// and test that deceased do not pay, but living ones (including infected) pays 1

		// Follows sec038_exponentialInfections

		/* Expected events :
		 * Screenings as usual, never successful
		 * sec 03 : 1 initial infection
		 * sec 08 : 1 spread
		 * sec 13 : 2 spreads
		 * sec 18 : 3 spreads, one death
		 * sec 20 : taxes (+99 = 99)
		 * sec 18 : 1 death, 3 spreads (6/99 infected)
		 * sec 23 : 1 death, 5 spreads (10/98 infected)
		 * sec 28 : 2 deaths, 8 spreads (16/96 infected)
		 * sec 33 : 3 deaths, 13 spreads (26/93 infected)
		 * sec 38 : 5 deaths, 21 spreads (42/88 infected)
		 * sec 40 : taxes (+88 = 187)
		 */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 39 + 1); // sec 00-38

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 5); // sec 23
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 8); // sec 28
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 13); // sec 33
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 21); // sec 38

		clock.advance(Duration.ofMillis(40000));
		simulation.update();

		assertEquals(99 + 88, simulation.getMoney());

		checkSelectorEmptiness();
	}

	@Test
	@Category(EdgeCase.class)
	public void money_increasedAtSec60_byInhabitants() {
		// Purpose : to confirm, taxes occurs *every* 20 seconds precisely
		// and test that deceased do not pay, but living ones (including infected) pays 1

		// Follows sec063_exponentialInfections_untilAllDead

		/* Expected events :
		 * Screenings as usual, never successful
		 * sec 03 : 1 initial infection
		 * sec 08 : 1 spread
		 * sec 13 : 2 spreads
		 * sec 18 : 3 spreads, one death
		 * sec 20 : taxes (+99 = 99)
		 * sec 18 : 1 death, 3 spreads (6/99 infected)
		 * sec 23 : 1 death, 5 spreads (10/98 infected)
		 * sec 28 : 2 deaths, 8 spreads (16/96 infected)
		 * sec 33 : 3 deaths, 13 spreads (26/93 infected)
		 * sec 38 : 5 deaths, 21 spreads (42/88 infected)
		 * sec 40 : taxes (+88 = 187)
		 * sec 43 : 8 deaths, 34 spreads (68/80 infected)
		 * sec 48 : 13 deaths, 12 spreads (67/67 infected) - can't infect more
		 * sec 53 : 21 deaths, 5 spreads (46/46 infected - 5 quarantined)
		 * sec 58 : 29 deaths, 4 spreads (16/16 infected - 1 quarantined)
		 * sec 60 : taxes (+16 = 203)
		 */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 47); // sec 00-47.8
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, 0, 25); // sec 48-52.8
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, 0, 25); // sec 53-57.8
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, 0, 5*2 + 1); // sec 58-60

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 5); // sec 23
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 8); // sec 28
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 13); // sec 33
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 21); // sec 38
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 34); // sec 43
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 12); // sec 48
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 4); // sec 53

		clock.advance(Duration.ofMillis(60000));
		simulation.update();

		assertEquals(99 + 88 + 16, simulation.getMoney());

		checkSelectorEmptiness();
	}


	@Test
	@Category(Version3.class)
	public void taxIncrease_firstTime_costs50() {
		// Same as money_increasedAtSec40_byInhabitants, but money is lowered by 50 at first and then gain is doubled

		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 19 + 1); // sec 00-20

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18

		clock.advanceTo(Duration.ofSeconds(20));
		simulation.update();
		simulation.executeOrder(OrderType.INCREASE_TAXES);

		assertEquals(99 - 50, simulation.getMoney());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version3.class)
	public void taxIncrease_firstTime_doubleRevenue() {
		// Same as money_increasedAtSec40_byInhabitants, but money is lowered by 50 at first and then gain is doubled

		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 39 + 1); // sec 00-40

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 5); // sec 23
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 8); // sec 28
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 13); // sec 33
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 21); // sec 38

		clock.advanceTo(Duration.ofSeconds(20));
		simulation.update();
		simulation.executeOrder(OrderType.INCREASE_TAXES);

		clock.advanceTo(Duration.ofSeconds(40));
		simulation.update();

		assertEquals(99 - 50 + 88 * 2, simulation.getMoney());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version3.class)
	public void taxIncrease_secondTime_costs50() {
		// Follows money_increasedAtSec40_byInhabitants

		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 39 + 1); // sec 00-40

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 5); // sec 23
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 8); // sec 28
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 13); // sec 33
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 21); // sec 38

		clock.advanceTo(Duration.ofSeconds(20));
		simulation.update();
		simulation.executeOrder(OrderType.INCREASE_TAXES);

		clock.advanceTo(Duration.ofSeconds(40));
		simulation.update();
		simulation.executeOrder(OrderType.INCREASE_TAXES);

		assertEquals(99 - 50 + 88 * 2 - 50, simulation.getMoney());

		checkSelectorEmptiness();
	}

	@Test
	@Category(EdgeCase.class)
	public void taxIncrease_secondTime_tripleRevenues() {
		// Follows money_increasedAtSec60_byInhabitants

		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 47); // sec 00-47.8
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, 0, 25); // sec 48-52.8
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, 0, 25); // sec 53-57.8
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, 0, 5*2 + 1); // sec 58-60

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 5); // sec 23
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 8); // sec 28
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 13); // sec 33
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 21); // sec 38
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 34); // sec 43
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 12); // sec 48
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 4); // sec 53

		clock.advanceTo(Duration.ofSeconds(20));
		simulation.update();
		simulation.executeOrder(OrderType.INCREASE_TAXES);

		clock.advanceTo(Duration.ofSeconds(40));
		simulation.update();
		simulation.executeOrder(OrderType.INCREASE_TAXES);

		clock.advanceTo(Duration.ofSeconds(60));
		simulation.update();

		assertEquals(99 - 50 + 88 * 2 - 50 + 16 * 3, simulation.getMoney());

		checkSelectorEmptiness();
	}




	@Test
	@Category(Version3.class)
	public void screeningCenter_firstTime_costs50() {
		// Same as money_increasedAtSec40_byInhabitants, but money is lowered by 50 at first and then gain is doubled

		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 19 + 1); // sec 00-20

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18

		clock.advanceTo(Duration.ofSeconds(20));
		simulation.update();
		simulation.executeOrder(OrderType.BUILD_SCREENING_CENTER);

		assertEquals(99 - 50, simulation.getMoney());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version3.class)
	public void screeningCenter_firstTime_doubleNextScreening_afterDelay() {
		// Follows money_increasedAtSec40_byInhabitants
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 19 + 1); // sec 00-20
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 5 * 15 - 2); // sec 20.2-34.6
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, 0, 1); // sec 34.8
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, 0, 2); // sec 35.0

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 5); // sec 23
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 8); // sec 28
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 13); // sec 33
		//selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 21); // sec 38

		clock.advanceTo(Duration.ofMillis(20000));
		simulation.update();
		simulation.executeOrder(OrderType.BUILD_SCREENING_CENTER);

		clock.advanceTo(Duration.ofMillis(34800));
		simulation.update();

		assertEquals(1, simulation.getQuarantinedPopulation());

		clock.advanceTo(Duration.ofMillis(35000));
		simulation.update();

		assertEquals(3, simulation.getQuarantinedPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version3.class)
	public void screeningCenter_firstTime_doubleAllScreenings() {
		// Follows money_increasedAtSec40_byInhabitants
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 19 + 1); // sec 00-20
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 5 * 15 - 2); // sec 20.2-34.6
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, 0, 1); // sec 34.8
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, 0, 2 * 6); // sec 36.0

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 5); // sec 23
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 8); // sec 28
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 13); // sec 33
		//selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 21); // sec 38

		clock.advanceTo(Duration.ofMillis(20000));
		simulation.update();
		simulation.executeOrder(OrderType.BUILD_SCREENING_CENTER);

		clock.advanceTo(Duration.ofMillis(36000));
		simulation.update();

		assertEquals(13, simulation.getQuarantinedPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(EdgeCase.class)
	public void screeningCenter_secondTime_costs50() {
		// Follows money_increasedAtSec40_byInhabitants

		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 19 + 1); // sec 00-20
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 5 * 15 - 1); // sec 20.2-34.8
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 52); // sec 35-40

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 5); // sec 23
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 8); // sec 28
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 13); // sec 33
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 21); // sec 38

		clock.advanceTo(Duration.ofSeconds(20));
		simulation.update();
		simulation.executeOrder(OrderType.BUILD_SCREENING_CENTER);

		clock.advanceTo(Duration.ofSeconds(40));
		simulation.update();
		simulation.executeOrder(OrderType.BUILD_SCREENING_CENTER);

		assertEquals(99 - 50 + 88 - 50, simulation.getMoney());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version4.class)
	public void researchMedicine_firstTime_costs50() {
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 19 + 1); // sec 00-20

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18

		clock.advanceTo(Duration.ofSeconds(20));
		simulation.update();
		simulation.executeOrder(OrderType.RESEARCH_MEDICINE);

		assertEquals(99 - 50, simulation.getMoney());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version4.class)
	public void researchMedicine_doNotImpactCuringTimeImmediately() {
		// Same events than sec038_exponentialInfections

		/* Expected events :
		 * Screenings as usual, never successful
		 * sec 03 : 1 initial infection (1/100 infected)
		 * sec 08 : 1 spread (2/100 infected)
		 * sec 13 : 2 spreads (4/100 infected)
		 * sec 18 : 1 death, 3 spreads (6/99 infected)
		 * sec 23 : 1 death, 5 spreads (10/98 infected)
		 * sec 28 : 2 deaths, 8 spreads (16/96 infected)
		 * sec 33 : 3 deaths, 13 spreads, 1 quarantine (26/93 infected, 1 quarantined)
		 * sec 38 : 1 cure, 5 deaths, 20 spreads (40/88 infected)
		 */

		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 32); // sec 00-32.8
		selector.enqueueRanks(SelectionContext.Screening, 5); // sec 33.0
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 4 + 1); // sec 33.2-38

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 5); // sec 23
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 8); // sec 28
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 13); // sec 33
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 20); // sec 38

		clock.advanceTo(Duration.ofSeconds(20));
		simulation.update();
		simulation.executeOrder(OrderType.RESEARCH_MEDICINE);

		clock.advanceTo(Duration.ofMillis(37900));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(93, simulation.getLivingPopulation());
		assertEquals(26, simulation.getInfectedPopulation());
		assertEquals(1, simulation.getQuarantinedPopulation());
		assertEquals(7, simulation.getDeadPopulation());

		clock.advanceTo(Duration.ofMillis(38000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(88, simulation.getLivingPopulation());
		assertEquals(40, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(12, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version4.class)
	public void researchMedicine_reduceCuringTimeAfterFifteenSeconds() {
		// Same events than sec038_exponentialInfections

		/* Expected events :
		 * Screenings as usual the last of the queue, never successful, except the last one
		 * sec 03 : 1 initial infection (1/100 infected)
		 * sec 08 : 1 spread (2/100 infected)
		 * sec 13 : 2 spreads (4/100 infected)
		 * sec 18 : 1 death, 3 spreads (6/99 infected)
		 * sec 23 : 1 death, 5 spreads (10/98 infected)
		 * sec 28 : 2 deaths, 8 spreads (16/96 infected)
		 * sec 33 : 3 deaths, 13 spreads (26/93 infected)
		 * sec 38 : 5 deaths, 21 spreads, 1 quarantine (42/88 infected, 1 quarantine)
		 * sec 40.5 : 1 cure (41/88 infected)
		 */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 37); // sec 00-37.8
		selector.enqueueRanks(SelectionContext.Screening, 0); // sec 38.0
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 3 + 1); // sec 38.2-42.0

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 5); // sec 23
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 8); // sec 28
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 13); // sec 33
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 21); // sec 38

		clock.advanceTo(Duration.ofSeconds(20));
		simulation.update();
		simulation.executeOrder(OrderType.RESEARCH_MEDICINE);

		clock.advanceTo(Duration.ofMillis(38000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(88, simulation.getLivingPopulation());
		assertEquals(42, simulation.getInfectedPopulation());
		assertEquals(1, simulation.getQuarantinedPopulation());
		assertEquals(12, simulation.getDeadPopulation());

		clock.advanceTo(Duration.ofMillis(41900));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(88, simulation.getLivingPopulation());
		assertEquals(42, simulation.getInfectedPopulation());
		assertEquals(1, simulation.getQuarantinedPopulation());
		assertEquals(12, simulation.getDeadPopulation());

		clock.advanceTo(Duration.ofMillis(42000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(88, simulation.getLivingPopulation());
		assertEquals(41, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(12, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version4.class)
	public void researchVaccine_firstTime_costs50() {
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 19 + 1); // sec 00-20

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18

		clock.advanceTo(Duration.ofSeconds(20));
		simulation.update();
		simulation.executeOrder(OrderType.RESEARCH_VACCINE);

		assertEquals(99 - 50, simulation.getMoney());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version4.class)
	public void researchVaccine_doNotImpactDyingTimeImmediately() {
		// Same events than sec038_exponentialInfections

		/* Expected events :
		 * Screenings as usual, never successful
		 * sec 03 : 1 initial infection (1/100 infected)
		 * sec 08 : 1 spread (2/100 infected)
		 * sec 13 : 2 spreads (4/100 infected)
		 * sec 18 : 1 death, 3 spreads (6/99 infected)
		 * sec 23 : 1 death, 5 spreads (10/98 infected)
		 * sec 28 : 2 deaths, 8 spreads (16/96 infected)
		 * sec 33 : 3 deaths, 13 spreads (26/93 infected)
		 * sec 38 : 5 deaths, 21 spreads (42/88 infected)
		 */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 32 + 1); // sec 00-38

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 5); // sec 23
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 8); // sec 28
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 13); // sec 33

		clock.advanceTo(Duration.ofSeconds(20));
		simulation.update();
		simulation.executeOrder(OrderType.RESEARCH_VACCINE);

		clock.advanceTo(Duration.ofMillis(28000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(96, simulation.getLivingPopulation());
		assertEquals(16, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(4, simulation.getDeadPopulation());

		clock.advanceTo(Duration.ofMillis(33000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(93, simulation.getLivingPopulation());
		assertEquals(26, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(7, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version4.class)
	public void researchVaccine_impactDyingTimeOfInfectedAfter5sec() {
		// Same events than sec048_exponentialInfections_untilAllInfected

		/* Expected events :
		 * Screenings as usual the last of the queue, never successful, except the last one
		 * sec 03 : 1 initial infection (1/100 infected)
		 * sec 08 : 1 spread (2/100 infected)
		 * sec 13 : 2 spreads (4/100 infected)
		 * sec 18 : 1 death, 3 spreads (6/99 infected)
		 * sec 23 : 1 death, 5 spreads (10/98 infected)
		 * sec 25 : Vaccine improved; next spreading will kill the host after 18,75 sec instead of 15 sec
		 * sec 28 : 2 deaths, 8 spreads (16/96 infected)
		 * sec 33 : 3 deaths, 13 spreads (26/93 infected)
		 * sec 38 : 5 deaths, 21 spreads (42/88 infected)
		 * sec 43 : 0 deaths, 42 spreads (84/88 infected)
		 * sec 46.75 : 8 deaths (76/80 infected)
		 */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 45 + 4); // sec 00-46.6

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 5); // sec 23
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 8); // sec 28
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 13); // sec 33
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 21); // sec 38
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 42); // sec 43

		clock.advanceTo(Duration.ofSeconds(20));
		simulation.update();
		simulation.executeOrder(OrderType.RESEARCH_VACCINE);

		clock.advanceTo(Duration.ofMillis(46749));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(88, simulation.getLivingPopulation());
		assertEquals(84, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(12, simulation.getDeadPopulation());

		clock.advanceTo(Duration.ofMillis(46750));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(80, simulation.getLivingPopulation());
		assertEquals(76, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(20, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version4.class)
	public void young_dieHalfAsQuick() {
		// Purpose : to confirm that young ones die after 7,5 sec

		// Note: this tests creates two simulations but discard the first one. This means creating a simulation that
		// is never ran should have no impact

		/* Expected events :
		 * sec 00 : 4 screenings
		 * sec 01 : 5 screenings
		 * sec 02 : 5 screenings
		 * sec 03 : 1 initial infection, 5 screenings
		 * sec 04 : 5 screenings
		 * sec 05 : 5 screenings
		 * sec 06 : 5 screenings
		 * sec 07 : 5 screenings
		 * sec 08 : 1 spread, 5 screenings
		 * sec 09 : 5 screenings
		 *
		 * sec 10.0 : 1 screening
		 * sec 10.2 : 1 screening
		 * sec 10.4 : 1 screening
		 * sec 10.5 : 1 death
		 */
		simulation = createSimulation(clock, selector, new BasicTraitsProvider(AgeCategory.YOUNG), 100);
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 9 + 3); // sec 00-10.5

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRanks(SelectionContext.Spreading, 1); // sec 08

		clock.advance(Duration.ofMillis(10500));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(99, simulation.getLivingPopulation());
		assertEquals(1, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(1, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version4.class)
	public void young_dieHalfAsQuick_andDoNotSpread() {
		// Purpose : to confirm that young ones die after 7,5 sec

		// Note: this tests creates two simulations but discard the first one. This means creating a simulation that
		// is never ran should have no impact

		/* Expected events :
		 * sec 00 : 4 screenings
		 * sec 01 : 5 screenings
		 * sec 02 : 5 screenings
		 * sec 03 : 1 initial infection, 5 screenings
		 * sec 04 : 5 screenings
		 * sec 05 : 5 screenings
		 * sec 06 : 5 screenings
		 * sec 07 : 5 screenings
		 * sec 08 : 1 spread, 5 screenings
		 * sec 09 : 5 screenings
		 *
		 * sec 10.0 : 1 screening
		 * sec 10.2 : 1 screening
		 * sec 10.4 : 1 screening
		 * sec 10.5 : 1 death
		 * sec 10.6 : 1 screening
		 * sec 10.8 : 1 screening
		 *
		 * sec 11 : 5 screenings
		 * sec 12 : 5 screenings
		 * sec 13 : 1 spread
		 */

		simulation = createSimulation(clock, selector, new BasicTraitsProvider(AgeCategory.YOUNG), 100);
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 12 + 1); // sec 00-13

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRanks(SelectionContext.Spreading, 1); // sec 08
		selector.enqueueRanks(SelectionContext.Spreading, 2); // sec 13

		clock.advance(Duration.ofMillis(13000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(99, simulation.getLivingPopulation());
		assertEquals(2, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(1, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(EdgeCase.class)
	public void young_tillSecond20() {
		// Purpose : to assess what happens before the first taxes

		// Note: this tests creates two simulations but discard the first one. This means creating a simulation that
		// is never ran should have no impact

		/* Expected events :
		 * sec 01 : 5 screenings
		 * sec 02 : 5 screenings
		 * sec 03 : 1 initial infection, 5 screenings
		 * sec 04 : 5 screenings
		 * sec 05 : 5 screenings
		 * sec 06 : 5 screenings
		 * sec 07 : 5 screenings
		 * sec 08 : 1 spread, 5 screenings
		 * sec 09 : 5 screenings
		 *
		 * sec 10.0 : 1 screening
		 * sec 10.2 : 1 screening
		 * sec 10.4 : 1 screening
		 * sec 10.5 : 1 death
		 * sec 10.6 : 1 screening
		 * sec 10.8 : 1 screening
		 *
		 * sec 11 : 5 screenings
		 * sec 12 : 5 screenings
		 * sec 13 : 1 spread, 5 screenings
		 * sec 14 : 5 screenings
		 *
		 * sec 15.0 : 1 screening
		 * sec 15.2 : 1 screening
		 * sec 15.4 : 1 screening
		 * sec 15.5 : 1 death
		 * sec 15.6 : 1 screening
		 * sec 15.8 : 1 screening
		 *
		 * sec 16 : 5 screenings
		 * sec 17 : 5 screenings
		 * sec 18 : 1 spread, 5 screenings
		 * sec 19 : 5 screenings
		 * sec 20.0 : 1 screening
		 */

		simulation = createSimulation(clock, selector, new BasicTraitsProvider(AgeCategory.YOUNG), 100);
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 19 + 1); // sec 00-20.0

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRanks(SelectionContext.Spreading, 1); // sec 08
		selector.enqueueRanks(SelectionContext.Spreading, 2); // sec 13
		selector.enqueueRanks(SelectionContext.Spreading, 3); // sec 18

		clock.advanceTo(Duration.ofMillis(15500));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(98, simulation.getLivingPopulation());
		assertEquals(1, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(2, simulation.getDeadPopulation());

		clock.advanceTo(Duration.ofMillis(20000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(98, simulation.getLivingPopulation());
		assertEquals(2, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(2, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(EdgeCase.class)
	public void young_tillSecond20_withOneSaved() {
		// Purpose : to assess what happens before the first taxes, with one inhabitant saved

		// Note: this tests creates two simulations but discard the first one. This means creating a simulation that
		// is never ran should have no impact

		/* Difference in contrast to before
		 * sec 08 : 1 spread - immediately screened
		 * After that, there are no more spreading nor deaths
		 */

		simulation = createSimulation(clock, selector, new BasicTraitsProvider(AgeCategory.YOUNG), 100);
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 7); // sec 00-07.8
		selector.enqueueRanks(SelectionContext.Screening, 1); // sec 08.0
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 11 + 1); // sec 08.2-20.0

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRanks(SelectionContext.Spreading, 0); // sec 08

		clock.advanceTo(Duration.ofMillis(8000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(100, simulation.getLivingPopulation());
		assertEquals(2, simulation.getInfectedPopulation());
		assertEquals(1, simulation.getQuarantinedPopulation());
		assertEquals(0, simulation.getDeadPopulation());

		clock.advanceTo(Duration.ofMillis(10500));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(99, simulation.getLivingPopulation());
		assertEquals(1, simulation.getInfectedPopulation());
		assertEquals(1, simulation.getQuarantinedPopulation());
		assertEquals(1, simulation.getDeadPopulation());

		clock.advanceTo(Duration.ofMillis(13000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(99, simulation.getLivingPopulation());
		assertEquals(0, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(1, simulation.getDeadPopulation());

		clock.advanceTo(Duration.ofMillis(20000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(99, simulation.getLivingPopulation());
		assertEquals(0, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(1, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version4.class)
	public void young_taxedHalfThanAdults() {
		// Purpose : to confirm, young inhabitants are taxed half the standard amount

		// Note: this tests creates two simulations but discard the first one. This means creating a simulation that
		// is never ran should have no impact

		simulation = createSimulation(clock, selector, new BasicTraitsProvider(AgeCategory.YOUNG), 100);
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 19 + 1); // sec 00-20.0

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRanks(SelectionContext.Spreading, 1); // sec 08
		selector.enqueueRanks(SelectionContext.Spreading, 2); // sec 13
		selector.enqueueRanks(SelectionContext.Spreading, 3); // sec 18

		clock.advance(Duration.ofMillis(20000));
		simulation.update();

		assertEquals(49, simulation.getMoney());
	}

	@Test
	@Category(Version4.class)
	public void young_taxedHalfThanAdults_roundedDown() {
		// Purpose : to confirm, taxes are rounded down (truncated).

		// Note: this tests creates two simulations but discard the first one. This means creating a simulation that
		// is never ran should have no impact

		simulation = createSimulation(clock, selector, new BasicTraitsProvider(AgeCategory.YOUNG), 100);
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 7); // sec 00-07.8
		selector.enqueueRanks(SelectionContext.Screening, 1); // sec 08.0
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 11 + 1); // sec 08.2-20.0

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRanks(SelectionContext.Spreading, 0); // sec 08

		clock.advance(Duration.ofMillis(20000));
		simulation.update();

		assertEquals(49, simulation.getMoney());
	}

	@Test
	@Category(Version4.class)
	public void senior_dieThriceAsQuickAndNeverSpread() {
		// Purpose : to confirm that seniors die after 5 sec
		// And also that they as such never spread

		// Note: this tests creates two simulations but discard the first one. This means creating a simulation that
		// is never ran should have no impact

		/* Expected events :
		 * sec 00 : 4 screenings
		 * sec 01 : 5 screenings
		 * sec 02 : 5 screenings
		 * sec 03 : 1 initial infection, 5 screenings
		 * sec 04 : 5 screenings
		 * sec 05 : 5 screenings
		 * sec 06 : 5 screenings
		 * sec 07 : 5 screenings
		 * sec 08 : 1 death, 1 screening
		 */
		simulation = createSimulation(clock, selector, new BasicTraitsProvider(AgeCategory.SENIOR), 100);
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 7 + 1); // sec 00-08

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);

		clock.advanceTo(Duration.ofMillis(8000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(99, simulation.getLivingPopulation());
		assertEquals(0, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(1, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(EdgeCase.class)
	public void senior_tillSecond20() {
		// Purpose : to assess what happens before the first taxes

		// Note: this tests creates two simulations but discard the first one. This means creating a simulation that
		// is never ran should have no impact

		/* Expected events : same as before, then only screenings
		 */
		simulation = createSimulation(clock, selector, new BasicTraitsProvider(AgeCategory.SENIOR), 100);
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 19 + 1); // sec 00-20

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);

		clock.advanceTo(Duration.ofMillis(20000));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(99, simulation.getLivingPopulation());
		assertEquals(0, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(1, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version4.class)
	public void senior_taxedLikeThreeQuarterOfAdults_roundedDown() {
		// Purpose : to confirm, young inhabitants are taxed half the standard amount

		// Note: this tests creates two simulations but discard the first one. This means creating a simulation that
		// is never ran should have no impact

		simulation = createSimulation(clock, selector, new BasicTraitsProvider(AgeCategory.SENIOR), 100);
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 19 + 1); // sec 00-20

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);

		clock.advance(Duration.ofMillis(20000));
		simulation.update();

		assertEquals(74, simulation.getMoney());
	}

	@Test
	@Category(Version4.class)
	public void sharedInterests_increaseChancesOfSimilarInhabitant() {
		// Purpose : to test whether the inhabitant

		/* Expected events :
		 * sec 00 : 4 screenings
		 * sec 01 : 5 screenings
		 * sec 02 : 5 screenings
		 * sec 03 : 1 initial infection, 5 screenings
		 * sec 04 : 5 screenings
		 * sec 05 : 5 screenings
		 * sec 06 : 5 screenings
		 * sec 07 : 5 screenings
		 * sec 08 : 1 spread, 1 screenings
		 */

		var traitsProvider = new BasicTraitsProvider(AgeCategory.ADULT);
		traitsProvider.setInterestsOf(0, Interests.NIGHT_LIFE);
		traitsProvider.setInterestsOf(1, Interests.NIGHT_LIFE);
		traitsProvider.setInterestsOf(2, Interests.CULTURE, Interests.SPORT);

		simulation = createSimulation(clock, selector, traitsProvider, 100);
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 7);
		selector.enqueueRanks(SelectionContext.Screening, 1); // sec 08 - #1 in the list of choice, hence inhabitant #1

		selector.enqueueRanks(SelectionContext.InitialInfection, 0); // #0
		selector.enqueueRanks(SelectionContext.Spreading, 1); // #1 in the list of choice (#1, #1, #2, #3, ...), hence inhabitant #1

		clock.advance(Duration.ofSeconds(8));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(100, simulation.getLivingPopulation());
		assertEquals(2, simulation.getInfectedPopulation());
		assertEquals(1, simulation.getQuarantinedPopulation());
		assertEquals(0, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version4.class)
	public void sharedInterests_doNotIncreaseChancesOfDissimilarInhabitant() {
		// Purpose : to test whether the inhabitant

		/* Expected events :
		 * sec 00 : 4 screenings
		 * sec 01 : 5 screenings
		 * sec 02 : 5 screenings
		 * sec 03 : 1 initial infection, 5 screenings
		 * sec 04 : 5 screenings
		 * sec 05 : 5 screenings
		 * sec 06 : 5 screenings
		 * sec 07 : 5 screenings
		 * sec 08 : 1 spread, 1 screenings
		 */

		var traitsProvider = new BasicTraitsProvider(AgeCategory.ADULT);
		traitsProvider.setInterestsOf(0, Interests.NIGHT_LIFE);
		traitsProvider.setInterestsOf(1, Interests.CULTURE, Interests.SPORT);
		traitsProvider.setInterestsOf(2, Interests.CULTURE, Interests.SPORT);

		simulation = createSimulation(clock, selector, traitsProvider, 100);
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 7);
		selector.enqueueRanks(SelectionContext.Screening, 1); // sec 08 - #1 in the list of choice, hence inhabitant #1

		selector.enqueueRanks(SelectionContext.InitialInfection, 0); // #0
		selector.enqueueRanks(SelectionContext.Spreading, 1); // #1 in the list of choice (#1, #2, #3, #4, ...), hence inhabitant #2

		clock.advance(Duration.ofSeconds(8));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(100, simulation.getLivingPopulation());
		assertEquals(2, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(0, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version4.class)
	public void sharedInterests_increaseChancesOfSimilarInhabitant_onlyByTwo() {
		// Purpose : to test whether the inhabitant

		/* Expected events :
		 * sec 00 : 4 screenings
		 * sec 01 : 5 screenings
		 * sec 02 : 5 screenings
		 * sec 03 : 1 initial infection, 5 screenings
		 * sec 04 : 5 screenings
		 * sec 05 : 5 screenings
		 * sec 06 : 5 screenings
		 * sec 07 : 5 screenings
		 * sec 08 : 1 spread, 1 screenings
		 */

		var traitsProvider = new BasicTraitsProvider(AgeCategory.ADULT);
		traitsProvider.setInterestsOf(0, Interests.NIGHT_LIFE);
		traitsProvider.setInterestsOf(1, Interests.NIGHT_LIFE);
		traitsProvider.setInterestsOf(2, Interests.CULTURE, Interests.SPORT);

		simulation = createSimulation(clock, selector, traitsProvider, 100);
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 7);
		selector.enqueueRanks(SelectionContext.Screening, 1); // sec 08 - #1 in the list of choice, hence inhabitant #1

		selector.enqueueRanks(SelectionContext.InitialInfection, 0); // #0
		selector.enqueueRanks(SelectionContext.Spreading, 2); // #2 in the list of choice (#1, #1, #2, #3, ...), hence inhabitant #2

		clock.advance(Duration.ofSeconds(8));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(100, simulation.getLivingPopulation());
		assertEquals(2, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(0, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version4.class)
	public void sharedInterests_increaseChancesOfSimilarInhabitant_onlyByTwo_evenWithMultipleSharedInterests() {
		// Purpose : to test whether the inhabitant

		/* Expected events :
		 * sec 00 : 4 screenings
		 * sec 01 : 5 screenings
		 * sec 02 : 5 screenings
		 * sec 03 : 1 initial infection, 5 screenings
		 * sec 04 : 5 screenings
		 * sec 05 : 5 screenings
		 * sec 06 : 5 screenings
		 * sec 07 : 5 screenings
		 * sec 08 : 1 spread, 1 screenings
		 */

		var traitsProvider = new BasicTraitsProvider(AgeCategory.ADULT);
		traitsProvider.setInterestsOf(0, Interests.NIGHT_LIFE, Interests.CULTURE);
		traitsProvider.setInterestsOf(1, Interests.NIGHT_LIFE, Interests.CULTURE);
		traitsProvider.setInterestsOf(2, Interests.SPORT);
		traitsProvider.setInterestsOf(2, Interests.SPORT);

		simulation = createSimulation(clock, selector, traitsProvider, 100);
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 7);
		selector.enqueueRanks(SelectionContext.Screening, 1); // sec 08 - #1 in the list of choice, hence inhabitant #1

		selector.enqueueRanks(SelectionContext.InitialInfection, 0); // #0
		selector.enqueueRanks(SelectionContext.Spreading, 2); // #2 in the list of choice (#1, #1, #2, #3, ...), hence inhabitant #2

		clock.advance(Duration.ofSeconds(8));
		simulation.update();

		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(100, simulation.getLivingPopulation());
		assertEquals(2, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getQuarantinedPopulation());
		assertEquals(0, simulation.getDeadPopulation());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version5.class)
	public void initialPanic_isTakenIntoAccount() {
		simulation = createSimulation(clock, selector, new BasicTraitsProvider(), 100, 54, null);

		assertEquals(54, simulation.getPanicLevel());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version5.class)
	public void initialInfection_increasesPanicByTwo() {
		selector.enqueueRanks(SelectionContext.InitialInfection, 0); // #0
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 2 + 1);

		simulation = createSimulation(clock, selector, new BasicTraitsProvider(), 100, 54, null);
		clock.advanceTo(Duration.ofSeconds(3));
		simulation.update();

		assertEquals(56, simulation.getPanicLevel());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version5.class)
	public void spreading_increasesPanicByTwo() {
		// Same scenario as sec008_twoPersonsAreInfected

		/* Expected events :
		 * sec 00 : 4 screenings
		 * sec 01 : 5 screenings
		 * sec 02 : 5 screenings
		 * sec 03 : 1 initial infection, 5 screenings
		 * sec 04 : 5 screenings
		 * sec 05 : 5 screenings
		 * sec 06 : 5 screenings
		 * sec 07 : 5 screenings
		 * sec 08 : 1 spread, 1 screenings
		 */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 7 + 1);
		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRanks(SelectionContext.Spreading, 1);

		simulation = createSimulation(clock, selector, new BasicTraitsProvider(), 100, 0, null);
		clock.advance(Duration.ofSeconds(8));
		simulation.update();

		assertEquals(2 // sec 03
						+ 1*2 // sec 08
				, simulation.getPanicLevel());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version5.class)
	public void everySpreading_increasePanicByTwo() {
		// Same scenario as sec013_fourPersonsAreInfected

		/* Expected events :
		 * sec 00 : 4 screenings
		 * sec 01 : 5 screenings
		 * sec 02 : 5 screenings
		 * sec 03 : 1 initial infection, 5 screenings
		 * sec 04 : 5 screenings
		 * sec 05 : 5 screenings
		 * sec 06 : 5 screenings
		 * sec 07 : 5 screenings
		 * sec 08 : 1 spread, 5 screenings
		 * sec 09 : 5 screenings
		 * sec 10 : 5 screenings
		 * sec 11 : 5 screenings
		 * sec 12 : 5 screenings
		 * sec 13 : 2 spreads, 1 screening
		 */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 12 + 1);
		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRanks(SelectionContext.Spreading, 1); // sec 08
		selector.enqueueRanks(SelectionContext.Spreading, 2, 4); // sec 13

		simulation = createSimulation(clock, selector, new BasicTraitsProvider(), 100, 0, null);
		clock.advance(Duration.ofSeconds(13));
		simulation.update();

		assertEquals(2 // sec 03
						+ 1*2 // sec 08
						+ 2*2 // sec 13
				, simulation.getPanicLevel());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version5.class)
	public void deaths_increasesPanicByFive() {
		// Same scenario as sec018_firstDeath
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 17 + 1); // sec 00-18

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRanks(SelectionContext.Spreading, 1); // sec 08
		selector.enqueueRanks(SelectionContext.Spreading, 2, 4); // sec 13
		selector.enqueueRanks(SelectionContext.Spreading, 5, 6, 7); // sec 18

		simulation = createSimulation(clock, selector, new BasicTraitsProvider(), 100, 0, null);
		clock.advance(Duration.ofSeconds(18));
		simulation.update();

		assertEquals(2 // sec 03
						+ 1*2 // sec 08
						+ 2*2 // sec 13
						+ 1*5 + 3*2 // sec 18
				, simulation.getPanicLevel());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version5.class)
	public void taxIncrease_increasesPanicByFive() {
		// Same scenario as taxIncrease_firstTime_doubleRevenue

		/* Expected events :
		 * Screenings as usual, never successful
		 * sec 03 : 1 initial infection
		 * sec 08 : 1 spread
		 * sec 13 : 2 spreads
		 * sec 18 : 3 spreads, one death
		 * sec 20 : taxes (+99 = 99)
		 * sec 23 : 1 death, 5 spreads (10/98 infected)
		 * sec 28 : 2 deaths, 8 spreads (16/96 infected)
		 * sec 33 : 3 deaths, 13 spreads (26/93 infected)
		 */

		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 34 + 1); // sec 00-40

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 5); // sec 23
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 8); // sec 28
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 13); // sec 33

		simulation = createSimulation(clock, selector, new BasicTraitsProvider(), 100, 0, null);
		clock.advanceTo(Duration.ofSeconds(20));
		simulation.update();
		simulation.executeOrder(OrderType.INCREASE_TAXES);

		assertEquals(2 // sec 03
						+ 1*2 // sec 08
						+ 2*2 // sec 13
						+ 1*5 + 3*2 // sec 18
				, simulation.getPanicLevel());

		clock.advanceTo(Duration.ofSeconds(35));
		simulation.update();

		assertEquals(2 // sec 03
				+ 1*2 // sec 08
				+ 2*2 // sec 13
				+ 1*5 + 3*2 // sec 18
				+ 1*5 + 5*2 // sec 23
				+ 2*5 + 8*2 // sec 28
				+ 3*5 + 13*2 // sec 35
				+ 5 // sec 33, includes tax increase
				, simulation.getPanicLevel());

		checkSelectorEmptiness();
	}

	@Test
	@Category(Version5.class)
	public void curing_reducesPanicByTwo() {
		// Same scenario as screening_early_preventsDeath

		/* Expected events :
		 * sec 00 : 4 screenings
		 * sec 01 : 5 screenings
		 * sec 02 : 5 screenings
		 * sec 03 : 1 initial infection, 5 screenings
		 * sec 04 : 5 screenings
		 * sec 05 : 5 screenings
		 * sec 06 : 5 screenings
		 * sec 07 : 5 screenings
		 * sec 08 : 1 screening
		 */
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 2); // sec 00
		selector.enqueueRanks(SelectionContext.Screening, 3); // sec 03.000
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 4 + 1); // sec 03.200-08

		selector.enqueueRanks(SelectionContext.InitialInfection, 3);

		simulation = createSimulation(clock, selector, new BasicTraitsProvider(), 100, 54, null);
		clock.advance(Duration.ofSeconds(8));
		simulation.update();

		assertEquals(54 + 2 - 2, simulation.getPanicLevel());

		checkSelectorEmptiness();
	}

	@Test(timeout=1000)
	@Category(Version5.class)
	public void network_connectsToMigrationServer() throws IOException, ClassNotFoundException, InterruptedException {
		try(FakeMigrationServer fakeMigrationServer = new FakeMigrationServer();
			Simulation simulation = createSimulation(
					clock, selector, new BasicTraitsProvider(),
					100, 54, fakeMigrationServer.getAddress())) {

			fakeMigrationServer.waitForAccept();
			HelloMessage message = fakeMigrationServer.readHello();

			assertNotNull(message.getName());
		}
	}

	@Test(timeout=1000)
	@Category(Version5.class)
	public void initialInfection_canTriggerEmigration() throws IOException, ClassNotFoundException, InterruptedException {
		selector.enqueueRanks(SelectionContext.InitialInfection, 0); // #0
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 2 + 1);

		selector.enqueueRanks(SelectionContext.Emigration, 1); // #1

		try(FakeMigrationServer fakeMigrationServer = new FakeMigrationServer();
			Simulation simulation = createSimulation(
					clock, selector, new BasicTraitsProvider(),
					100, 99, fakeMigrationServer.getAddress())) {

			fakeMigrationServer.waitForAccept();
			fakeMigrationServer.readHello();

			clock.advanceTo(Duration.ofSeconds(3));
			simulation.update();

			EmigrationMessage message = fakeMigrationServer.readMigration();
			assertFalse(message.isInfected());
		}
	}

	@Test(timeout=1000)
	@Category(Version5.class)
	public void initialInfection_canTriggerEmigration_ofInfected() throws IOException, ClassNotFoundException, InterruptedException {
		selector.enqueueRanks(SelectionContext.InitialInfection, 0); // #0
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 2 + 1);

		selector.enqueueRanks(SelectionContext.Emigration, 0); // #1

		try(FakeMigrationServer fakeMigrationServer = new FakeMigrationServer();
			Simulation simulation = createSimulation(
					clock, selector, new BasicTraitsProvider(),
					100, 99, fakeMigrationServer.getAddress())) {
			fakeMigrationServer.waitForAccept();
			fakeMigrationServer.readHello();

			clock.advanceTo(Duration.ofSeconds(3));
			simulation.update();

			EmigrationMessage message = fakeMigrationServer.readMigration();
			assertTrue(message.isInfected());
		}
	}

	@Test(timeout=1000)
	@Category(Version5.class)
	public void emigration_reducesPanic() throws IOException, ClassNotFoundException, InterruptedException {
		selector.enqueueRanks(SelectionContext.InitialInfection, 0); // #0
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 2 + 1);

		selector.enqueueRanks(SelectionContext.Emigration, 1); // #1

		try(FakeMigrationServer fakeMigrationServer = new FakeMigrationServer();
			Simulation simulation = createSimulation(
					clock, selector, new BasicTraitsProvider(),
					100, 99, fakeMigrationServer.getAddress())) {

			fakeMigrationServer.waitForAccept();
			fakeMigrationServer.readHello();

			clock.advanceTo(Duration.ofSeconds(3));
			simulation.update();

			assertEquals(99 + 2 - 5, simulation.getPanicLevel());
		}
	}

	@Test(timeout=1000)
	@Category(Version5.class)
	public void spreading_canTriggerEmigration() throws IOException, ClassNotFoundException, InterruptedException {
		// Same scenario as spreading_increasesPanicByTwo

		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 7 + 1);
		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRanks(SelectionContext.Spreading, 1);

		selector.enqueueRanks(SelectionContext.Emigration, -1);

		try(FakeMigrationServer fakeMigrationServer = new FakeMigrationServer();
			Simulation simulation = createSimulation(
					clock, selector, new BasicTraitsProvider(),
					100, 97, fakeMigrationServer.getAddress())) {
			fakeMigrationServer.waitForAccept();
			assertNotNull(fakeMigrationServer.readHello());

			// Panic will increase of 4, hence initial panic of 97 will make it gets to 101, then to 96

			clock.advance(Duration.ofSeconds(8));
			simulation.update();

			assertNotNull(fakeMigrationServer.readMigration());
			assertEquals(96, simulation.getPanicLevel());
			checkSelectorEmptiness();
		}
	}

	@Test(timeout=1000)
	@Category(Version5.class)
	public void everySpreading_canTriggerEmigration() throws IOException, ClassNotFoundException, InterruptedException {
		// Same scenario as everySpreading_increasePanicByTwo

		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 12 + 1);
		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRanks(SelectionContext.Spreading, 1); // sec 08
		selector.enqueueRanks(SelectionContext.Spreading, 2, 4); // sec 13

		selector.enqueueRanks(SelectionContext.Emigration, -1);

		try(FakeMigrationServer fakeMigrationServer = new FakeMigrationServer();
			Simulation simulation = createSimulation(
					clock, selector, new BasicTraitsProvider(),
					100, 94, fakeMigrationServer.getAddress())) {
			fakeMigrationServer.waitForAccept();
			assertNotNull(fakeMigrationServer.readHello());

			clock.advance(Duration.ofSeconds(13));
			simulation.update();

			// Panic will increase of 8, hence initial panic of 94 will make it gets to 102, then to 97
			assertNotNull(fakeMigrationServer.readMigration());
			assertEquals(97, simulation.getPanicLevel());
			checkSelectorEmptiness();
		}
	}

	@Test(timeout=1000)
	@Category(EdgeCase.class)
	public void longScenario_canTriggerMultipleEmigrations() throws IOException, ClassNotFoundException, InterruptedException {
		// Same scenario as taxIncrease_increasesPanicByFive

		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 32 + 1); // sec 00-40

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 5); // sec 23
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 8); // sec 28
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 13); // sec 33

		selector.enqueueRanks(SelectionContext.Emigration, -1);
		selector.enqueueRanks(SelectionContext.Emigration, -1);

		try(FakeMigrationServer fakeMigrationServer = new FakeMigrationServer();
			Simulation simulation = createSimulation(
					clock, selector, new BasicTraitsProvider(),
					100, 0, fakeMigrationServer.getAddress())) {
			fakeMigrationServer.waitForAccept();
			assertNotNull(fakeMigrationServer.readHello());

			clock.advanceTo(Duration.ofSeconds(20));
			simulation.update();
			simulation.executeOrder(OrderType.INCREASE_TAXES);

			clock.advanceTo(Duration.ofSeconds(33));
			simulation.update();

			// At second 33
			// - population is initially 96
			// - there will be 3 deaths (+15 panic = 75, population = 93)
			// - there will be 13 spreads
			// 		- there will be 10 spreads (+20 panic = 95, population = 93)
			// 		- there will be an emigration (-5 panic = 90, population = 92)
			// 		- there will be 2 additional spreads (+4 panic = 94, population = 92)
			// 		- there will be an emigration (-5 panic = 79, population = 91)
			// 		- there will be 1 last spread (+2 panic = 91, population = 91)
			assertEquals(91, simulation.getPanicLevel());
			assertEquals(91, simulation.getLivingPopulation());
			assertNotNull(fakeMigrationServer.readMigration());
			assertNotNull(fakeMigrationServer.readMigration());

			assertEquals(91, simulation.getLivingPopulation());

			checkSelectorEmptiness();
		}
	}

	@Test(timeout=1000)
	@Category(EdgeCase.class)
	public void taxIncrease_canTriggerEmigration() throws IOException, ClassNotFoundException, InterruptedException {
		// Same scenario as taxIncrease_increasesPanicByFive, follows longScenario_canTriggerMultipleEmigrations

		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 34 + 1); // sec 00-40

		selector.enqueueRanks(SelectionContext.InitialInfection, 0);
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 1); // sec 08
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 2); // sec 13
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 3); // sec 18
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 5); // sec 23
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 8); // sec 28
		selector.enqueueRankMultipleTimes(SelectionContext.Spreading, 0, 13); // sec 33

		selector.enqueueRanks(SelectionContext.Emigration, -1);
		selector.enqueueRanks(SelectionContext.Emigration, -1);
		selector.enqueueRanks(SelectionContext.Emigration, -1);
		selector.enqueueRanks(SelectionContext.Emigration, -1);

		try(FakeMigrationServer fakeMigrationServer = new FakeMigrationServer();
			Simulation simulation = createSimulation(
					clock, selector, new BasicTraitsProvider(),
					100, 0, fakeMigrationServer.getAddress())) {
			fakeMigrationServer.waitForAccept();
			assertNotNull(fakeMigrationServer.readHello());

			clock.advanceTo(Duration.ofSeconds(20));
			simulation.update();
			simulation.executeOrder(OrderType.INCREASE_TAXES);

			clock.advanceTo(Duration.ofSeconds(33));
			simulation.update();

			assertNotNull(fakeMigrationServer.readMigration());
			assertNotNull(fakeMigrationServer.readMigration());

			clock.advanceTo(Duration.ofSeconds(35));
			simulation.update();

			// At second 35:
			// - there will be a tax increase (+5 panic = 96, population = 91)
			// - there will be an emigration (-5 panic = 91, population = 90)
			// - there will be a second emigration because the new manic level is above the new population
			//		(-5 panic = 86, population = 89)

			assertEquals(89, simulation.getLivingPopulation());
			assertEquals(86, simulation.getPanicLevel());
			assertNotNull(fakeMigrationServer.readMigration());
			assertNotNull(fakeMigrationServer.readMigration());
			checkSelectorEmptiness();
		}
	}

	@Test(timeout=1000)
	@Category(Version5.class)
	public void immigration_doesNotImmediatelyInfect() throws IOException, ClassNotFoundException, InterruptedException {
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 1 + 1); // sec 00-02.0

		try(FakeMigrationServer fakeMigrationServer = new FakeMigrationServer();
			Simulation simulation = createSimulation(
					clock, selector, new BasicTraitsProvider(),
					100, 0, fakeMigrationServer.getAddress())) {

			fakeMigrationServer.waitForAccept();
			assertNotNull(fakeMigrationServer.readHello());

			clock.advanceTo(Duration.ofSeconds(2));
			simulation.update();

			fakeMigrationServer.sendMessage(new ImmigrationMessage("Tests", false));
			Thread.sleep(200);
			assertEquals(100, simulation.getLivingPopulation());
			assertEquals(0, simulation.getInfectedPopulation());
		}
	}

	@Test(timeout=1000)
	@Category(Version5.class)
	public void immigration_addsNotInfectedPopulationIfRequested() throws IOException, ClassNotFoundException, InterruptedException {
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 4 + 1); // sec 00-05.0
		selector.enqueueRanks(SelectionContext.InitialInfection, 0);

		try(FakeMigrationServer fakeMigrationServer = new FakeMigrationServer();
			Simulation simulation = createSimulation(
					clock, selector, new BasicTraitsProvider(),
					100, 0, fakeMigrationServer.getAddress())) {

			fakeMigrationServer.waitForAccept();
			assertNotNull(fakeMigrationServer.readHello());

			clock.advanceTo(Duration.ofSeconds(2));
			simulation.update();

			fakeMigrationServer.sendMessage(new ImmigrationMessage("Tests", false));
			Thread.sleep(200);

			clock.advanceTo(Duration.ofSeconds(5));
			simulation.update();
			assertEquals(101, simulation.getLivingPopulation());
			assertEquals(1, simulation.getInfectedPopulation());
		}
	}

	@Test(timeout=1000)
	@Category(Version5.class)
	public void immigration_addsInfectedPopulationIfRequested() throws IOException, ClassNotFoundException, InterruptedException {
		selector.enqueueRankMultipleTimes(SelectionContext.Screening, -1, 4 + 5 * 4 + 1); // sec 00-05.0
		selector.enqueueRanks(SelectionContext.InitialInfection, 0);

		try(FakeMigrationServer fakeMigrationServer = new FakeMigrationServer();
			Simulation simulation = createSimulation(
					clock, selector, new BasicTraitsProvider(),
					100, 0, fakeMigrationServer.getAddress())) {

			fakeMigrationServer.waitForAccept();
			assertNotNull(fakeMigrationServer.readHello());

			clock.advanceTo(Duration.ofSeconds(2));
			simulation.update();

			fakeMigrationServer.sendMessage(new ImmigrationMessage("Tests", true));
			Thread.sleep(200);

			clock.advanceTo(Duration.ofSeconds(5));
			simulation.update();
			assertEquals(101, simulation.getLivingPopulation());
			assertEquals(2, simulation.getInfectedPopulation());
		}
	}
}
