package fr.efrei.paumier.shared.traits;

import java.util.*;

/**
 * This TraitsProvider sets up all inhabitant in the same way
 * They are all of a single age category (adults by default)
 * and have no interests by default
 */
public class BasicTraitsProvider implements TraitsProvider {
    private final AgeCategory ageCategory;
    private final HashMap<Integer, List<Interests>> interestsPerId = new HashMap<>();

    public BasicTraitsProvider() {
        this(AgeCategory.ADULT);
    }

    public BasicTraitsProvider(AgeCategory ageCategory) {
        this.ageCategory = ageCategory;
    }

    @Override
    public AgeCategory getAgeCategory(int inhabitantId) {
        return this.ageCategory;
    }

    @Override
    public List<Interests> getInterests(int inhabitantId) {
        return interestsPerId.containsKey(inhabitantId)
                ? interestsPerId.get(inhabitantId)
                : new ArrayList<>();
    }

    public void setInterestsOf(int inhabitantId, Interests... interests) {
        interestsPerId.put(inhabitantId,
                Collections.unmodifiableList(Arrays.asList(interests)));
    }
}
