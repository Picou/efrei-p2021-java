# Rapport de notation

## Qualité

* Nommage variables: 1
* Usage adéquat des fonctionnalités du langage: 1
* Simplicité des fonctions: 0.6
* Simplicité des classes: 1
* Pas de dupplication de code: 0.8
* Séparation de responsabilités: 0.9
* Encapsulation: 1
* Intégration dans une application: 1

Coefficient de qualité = 91.25%

## Versions

### Version 0

* Total: 15
* Succès: 15
* Taux: 1

### Version 1

* Total: 11
* Succès: 11
* Taux: 1

### Version 2

* Total: 6
* Succès: 6
* Taux: 1

### Version 3

* Total: 11
* Succès: 11
* Taux: 1

### Version 4

* Total: 16
* Succès: 16
* Taux: 1

### Version 5

* Total: 16
* Succès: 16
* Taux: 1

### Version 6

* Total: 6
* Succès: 5
* - Tourne: NOK
* - Stats Total: OK
* - Stats par Age: OK
* - Stats par Intérêts: OK
* - Affichage argent: OK
* - Execution fonctionnelle: OK
* Taux: 0.833333

### Total rendus

Taux moyen = 0.97619

Note intermédiaire = 9.76/10

## Rendu complet

* Total: 73
* Succès: 73
* Taux: 1

Note intermédiaire = 10/10

## Total 

Sous-total: 19.76/20 

Impact qualité: 95.63%

Total: 18.9/20 

