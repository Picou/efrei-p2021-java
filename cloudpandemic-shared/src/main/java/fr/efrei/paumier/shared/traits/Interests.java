package fr.efrei.paumier.shared.traits;

public enum Interests {
    SPORT, CULTURE, NIGHT_LIFE
}
