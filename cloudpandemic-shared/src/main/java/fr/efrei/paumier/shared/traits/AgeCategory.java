package fr.efrei.paumier.shared.traits;

public enum AgeCategory {
    YOUNG, ADULT, SENIOR
}
