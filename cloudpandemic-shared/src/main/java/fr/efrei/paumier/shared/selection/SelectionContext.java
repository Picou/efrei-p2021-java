package fr.efrei.paumier.shared.selection;

public enum SelectionContext {
    Screening,
    InitialInfection,
    Spreading,
    Emigration
}
