package fr.efrei.paumier.shared.networking;

import java.io.Serializable;

public class ImmigrationMessage implements Serializable {
    private static final long serialVersionUID = 4560083501581816567L;

    private final String origin;
    private final boolean infectedFlag;

    public ImmigrationMessage(String origin, boolean isInfected) {
        this.origin = origin;
        this.infectedFlag = isInfected;
    }

    public String getOrigin() {
        return this.origin;
    }

    public boolean isInfected() {
        return this.infectedFlag;
    }
}
