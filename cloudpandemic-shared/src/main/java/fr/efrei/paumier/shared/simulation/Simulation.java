package fr.efrei.paumier.shared.simulation;

import fr.efrei.paumier.shared.orders.OrderType;
import fr.efrei.paumier.shared.time.TimeManager;

import java.io.Closeable;

public interface Simulation extends TimeManager, Closeable {

	int getOriginalPopulation();
	int getLivingPopulation();
	int getInfectedPopulation();
	int getQuarantinedPopulation();
	int getDeadPopulation();

	long getMoney();
	int getPanicLevel();

	void executeOrder(OrderType order);
}
