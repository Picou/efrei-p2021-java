package fr.efrei.paumier.shared.networking;

import java.io.Serializable;

public class EmigrationMessage implements Serializable {
    private static final long serialVersionUID = 4560083501581816566L;

    private final boolean infectedFlag;

    public EmigrationMessage(boolean isInfected) {
        this.infectedFlag = isInfected;
    }

    public boolean isInfected() {
        return this.infectedFlag;
    }
}
