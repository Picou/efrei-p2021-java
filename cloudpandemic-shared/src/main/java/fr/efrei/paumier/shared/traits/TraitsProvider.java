package fr.efrei.paumier.shared.traits;

import java.util.List;

public interface TraitsProvider {
    AgeCategory getAgeCategory(int inhabitantId);
    List<Interests> getInterests(int inhabitantId);
}
