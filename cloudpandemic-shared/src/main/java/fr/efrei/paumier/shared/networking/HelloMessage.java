package fr.efrei.paumier.shared.networking;

import java.io.Serializable;

public class HelloMessage implements Serializable {
    private static final long serialVersionUID = 4343009205967193737L;

    private final String simulationName;

    public HelloMessage(String simulationName) {
        this.simulationName = simulationName;
    }

    public String getName() {
        return this.simulationName;
    }
}
