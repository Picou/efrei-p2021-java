package fr.efrei.paumier.shared.orders;

public enum OrderType {
    BUILD_SCREENING_CENTER,
    INCREASE_TAXES,
    RESEARCH_MEDICINE,
    RESEARCH_VACCINE
}
