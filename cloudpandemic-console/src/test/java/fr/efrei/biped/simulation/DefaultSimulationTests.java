package fr.efrei.biped.simulation;

import fr.efrei.paumier.shared.selection.Selector;
import fr.efrei.paumier.shared.simulation.BaseSimulationTests;
import fr.efrei.paumier.shared.simulation.Simulation;
import fr.efrei.paumier.shared.traits.TraitsProvider;

import java.net.InetSocketAddress;
import java.time.Clock;

public class DefaultSimulationTests extends BaseSimulationTests {

    @Override
    protected Simulation createSimulation(Clock clock, Selector selector, TraitsProvider traitsProvider, int population, int initialPanic, InetSocketAddress serverAddress) {
        return new DefaultSimulation(clock, selector, traitsProvider, population, initialPanic, serverAddress);
    }
}
