package fr.efrei.biped.engine;

import fr.efrei.paumier.shared.engine.BaseGameEngineTests;
import fr.efrei.paumier.shared.engine.GameEngine;

import java.time.Clock;

public class DefaultGameEngineTests extends BaseGameEngineTests {

    @Override
    protected GameEngine createGameEngine(Clock clock) {
        return new DefaultGameEngine(clock);
    }
}
