package fr.efrei.biped.simulation;

import fr.efrei.biped.engine.DefaultGameEngine;
import fr.efrei.biped.entities.City;
import fr.efrei.biped.entities.InhabitantState;
import fr.efrei.biped.events.*;
import fr.efrei.biped.networking.ImmigrationInput;
import fr.efrei.paumier.shared.engine.GameEngine;
import fr.efrei.paumier.shared.networking.HelloMessage;
import fr.efrei.paumier.shared.orders.OrderType;
import fr.efrei.paumier.shared.selection.Selector;
import fr.efrei.paumier.shared.traits.AgeCategory;
import fr.efrei.paumier.shared.traits.Interests;
import fr.efrei.paumier.shared.traits.TraitsProvider;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;

/**
 * Simulation du jeu, utilise le moteur d'évènements et une ville
 */
public class DefaultSimulation implements DetailedSimulation {

    private final GameEngine engine;
    private final City city;
    private ImmigrationInput immigrationInput;
    private Socket socket;
    private static final int UPGRADE_PRICE = 50;

    public DefaultSimulation(Clock clock, Selector selector, TraitsProvider traitsProvider, int nbInhabitants, int initialPanic, InetSocketAddress serverAdress) {
        ObjectOutputStream objectOutputStream = null;
        try {
            if (serverAdress != null) {
                this.socket = new Socket();
                socket.connect(serverAdress);
                objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                objectOutputStream.writeObject(
                        new HelloMessage("j'aimerais savoir comment faire une bonne purée quel est votre secret ?"));


            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.engine = new DefaultGameEngine(clock);
        this.city = new City(nbInhabitants, this.engine, traitsProvider, selector, initialPanic, objectOutputStream);
        this.engine.register(new Infect(Duration.ofSeconds(3), this.city));
        this.engine.register(new Taxing(Duration.ofSeconds(20), this.city));
        this.engine.register(new Screening(Duration.ofMillis(200), this.city));

        try {
            if (serverAdress != null) {
                ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
                this.immigrationInput = new ImmigrationInput(this.engine, this.city, objectInputStream);
                this.immigrationInput.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void update() {
        engine.update();

    }

    @Override
    public long getMoney() {
        return this.city.getFunds();
    }

    @Override
    public int getPanicLevel() {
        return this.city.getPanicLevel();
    }

    @Override
    public void executeOrder/*66*/(OrderType order) {
        synchronized (this) {

            switch (order) {
                case INCREASE_TAXES:
                    if (this.city.canBuyUpgrade(UPGRADE_PRICE)) {
                        this.city.buyUpgrade(UPGRADE_PRICE);
                        this.engine.register(new IncreasingTaxes(Duration.ofSeconds(15), this.city));
                    }
                    break;
                case BUILD_SCREENING_CENTER:
                    if (this.city.canBuyUpgrade(UPGRADE_PRICE)) {
                        this.city.buyUpgrade(UPGRADE_PRICE);
                        this.engine.register(new BuildingClinic(Duration.ofSeconds(15), this.city));
                    }
                    break;
                case RESEARCH_VACCINE:
                    if (this.city.canBuyUpgrade(UPGRADE_PRICE)) {
                        this.city.buyUpgrade(UPGRADE_PRICE);
                        this.engine.register(new ResearchVaccine(Duration.ofSeconds(5), this.city));
                    }
                    break;
                case RESEARCH_MEDICINE:
                    if (this.city.canBuyUpgrade(UPGRADE_PRICE)) {
                        this.city.buyUpgrade(UPGRADE_PRICE);
                        this.engine.register(new ResearchMedicine(Duration.ofSeconds(15), this.city));
                    }
                    break;
            }
        }
    }

    @Override
    public Instant getCurrentInstant() {
        return engine.getCurrentInstant();
    }

    @Override
    public int getDeadPopulation() {
        return city.getOriginalPopulation() - city.getLivingPopulation();
    }

    @Override
    public int getQuarantinedPopulation() {
        return city.getQuarantinedPopulation();
    }

    @Override
    public int getInfectedPopulation() {
        return city.getInfectedPopulation();
    }

    @Override
    public int getLivingPopulation() {
        return city.getLivingPopulation();
    }

    @Override
    public int getOriginalPopulation() {
        return city.getOriginalPopulation();
    }

    @Override
    public String displayLivingPopulationByAge() {
        return this.city.getLivingPopulationByAge(AgeCategory.YOUNG) + "/"
                + this.city.getLivingPopulationByAge(AgeCategory.ADULT) + "/"
                + this.city.getLivingPopulationByAge(AgeCategory.SENIOR);
    }

    @Override
    public String displayLivingPopulationByInterests() {
        return this.city.getLivingPopulationByInterests(Interests.SPORT) + "/"
                + this.city.getLivingPopulationByInterests(Interests.CULTURE) + "/"
                + this.city.getLivingPopulationByInterests(Interests.NIGHT_LIFE);
    }

    @Override
    public String displayInfectedPopulationByAge() {
        return this.city.getInfectedPopulationByAge(AgeCategory.YOUNG) + "/"
                + this.city.getInfectedPopulationByAge(AgeCategory.ADULT) + "/"
                + this.city.getInfectedPopulationByAge(AgeCategory.SENIOR);
    }

    @Override
    public String displayInfectedPopulationByInterests() {
        return this.city.getInfectedPopulationByInterests(Interests.SPORT) + "/"
                + this.city.getInfectedPopulationByInterests(Interests.CULTURE) + "/"
                + this.city.getInfectedPopulationByInterests(Interests.NIGHT_LIFE);
    }

    @Override
    public String displayQuarantinedPopulationByAge() {
        return this.city.getQuarantinedPopulationByAge(AgeCategory.YOUNG) + "/"
                + this.city.getQuarantinedPopulationByAge(AgeCategory.ADULT) + "/"
                + this.city.getQuarantinedPopulationByAge(AgeCategory.SENIOR);
    }

    @Override
    public String displayQuarantinedPopulationByInterests() {
        return this.city.getQuarantinedPopulationByInterests(Interests.SPORT) + "/"
                + this.city.getQuarantinedPopulationByInterests(Interests.CULTURE) + "/"
                + this.city.getQuarantinedPopulationByInterests(Interests.NIGHT_LIFE);
    }

    @Override
    public long getNbInhabitant(AgeCategory ageCategory, InhabitantState inhabitantState) {
        return this.city.getNbInhabitants(ageCategory, inhabitantState);
    }

    @Override
    public long getNbInhabitant(Interests interests, InhabitantState inhabitantState) {
        return this.city.getNbInhabitants(interests, inhabitantState);
    }

    @Override
    public int getTotalNbInhabitant(InhabitantState inhabitantState) {
        return this.city.getNbInhabitants(inhabitantState);
    }

    @Override
    public void close() {
        if(this.socket != null) {
            try {
                this.immigrationInput.stop();
                //this.immigrationInput.join();
                this.socket.close();
            } catch (IOException/* | InterruptedException */e) {
                e.printStackTrace();
            }
        }
    }
}
