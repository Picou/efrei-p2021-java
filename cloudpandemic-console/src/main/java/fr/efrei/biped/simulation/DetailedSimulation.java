package fr.efrei.biped.simulation;

import fr.efrei.biped.entities.InhabitantState;
import fr.efrei.paumier.shared.simulation.Simulation;
import fr.efrei.paumier.shared.traits.AgeCategory;
import fr.efrei.paumier.shared.traits.Interests;

public interface DetailedSimulation extends Simulation {
    String displayLivingPopulationByAge();
    String displayLivingPopulationByInterests();
    String displayInfectedPopulationByAge();
    String displayInfectedPopulationByInterests();
    String displayQuarantinedPopulationByAge();
    String displayQuarantinedPopulationByInterests();
    long getNbInhabitant(AgeCategory ageCategory, InhabitantState inhabitantState);
    long getNbInhabitant(Interests interests, InhabitantState inhabitantState);
    int getTotalNbInhabitant(InhabitantState inhabitantState);
}
