package fr.efrei.biped.start;

import fr.efrei.biped.traits.RandomTraitsProvider;
import fr.efrei.biped.selection.RandomSelector;
import fr.efrei.biped.simulation.DefaultSimulation;
import fr.efrei.biped.time.DefaultClock;
import fr.efrei.biped.userInterface.Console;
import fr.efrei.biped.userInterface.MainWindow;


public class Start {
    public static void main(String[] args) {
        //TODO recuperer l'adresse du serveur
        DefaultSimulation sim = new DefaultSimulation(new DefaultClock(), new RandomSelector(), new RandomTraitsProvider(), 100, 0, null);

        MainWindow window = new MainWindow(sim);

        window.refresh();
        window.setVisible(true);

        //refreshing window every second
        while (!Thread.interrupted()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            window.refresh();
        }


        //Thread t = new Thread(new Console(sim));
        //t.start();
    }
}
