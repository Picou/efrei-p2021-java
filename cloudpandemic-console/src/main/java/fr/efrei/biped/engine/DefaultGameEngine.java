package fr.efrei.biped.engine;

import fr.efrei.paumier.shared.engine.GameEngine;
import fr.efrei.paumier.shared.events.Event;

import java.time.Clock;
import java.time.Instant;
import java.util.*;

public class DefaultGameEngine implements GameEngine {
    private final Clock clock;
    private final List<RegisteredEvent> events;
    private Instant currentInstant;

    public DefaultGameEngine(Clock clock) {
        this.clock = clock;
        this.events = new ArrayList<>();
        this.currentInstant = this.clock.instant();
    }

    /**
     * @param event
     * Enregistre autant d'Event que souhaité
     * Tri les évènements en fonction de leur date de déclenchement
     */
    @Override
    public void register(Event... event) {
        for (Event e : event) {
            this.events.add(new RegisteredEvent(e, this.getCurrentInstant()));
        }
        this.events.sort(Comparator.comparing(RegisteredEvent::getTriggeringInstant));
    }

    /**
     * Trigger les évènements en fonction de leur instant de déclenchement
     * Enlève les évènements déclenchés de la liste d'évènements
     */
    @Override
    public void update() {
        this.currentInstant = clock.instant();

        RegisteredEvent e;
        for (int i = 0; i < events.size(); i++) {
            e = events.get(i);
            if(e.getTriggeringInstant().compareTo(getCurrentInstant()) <= 0) {

                //set currentInstant à l'instant de déclenchement de l'évèmenement
                currentInstant = e.getTriggeringInstant();

                e.trigger();

                //remet currentInstant à l'instant actuel
                currentInstant = clock.instant();

                events.remove(e);
                i--;
            }
        }
    }
    @Override
    public Instant getCurrentInstant() {
        return this.currentInstant;
    }

}