package fr.efrei.biped.engine;

import fr.efrei.paumier.shared.events.Event;

import java.time.Instant;

/**
 * Classe contenant un Event et sa date de départ
 */
class RegisteredEvent {

    private final Event event;
    private final Instant startEventInstant;

    public RegisteredEvent(Event e, Instant currentInstant) {
        this.event = e;
        this.startEventInstant = currentInstant;
    }

    public Instant getTriggeringInstant() {
        return this.startEventInstant.plus(this.event.getDuration());
    }

    public void trigger() {
        this.event.trigger();
    }

    @Override
    public String toString() {
        return event.toString() + getTriggeringInstant().toString();
    }
}
