package fr.efrei.biped.selection;

import fr.efrei.paumier.shared.selection.SelectionContext;
import fr.efrei.paumier.shared.selection.Selector;

import java.util.List;
import java.util.Random;

public class RandomSelector implements Selector {

    /**
     * @param choices liste pour la selection
     * @param context contexte de la selection
     * @param <TItem> type d'objet a selectionner
     * @return élément choisi
     * Séléctionne aléatoirement un élément dans une liste
     */
    @Override
    public <TItem> TItem selectAmong(List<TItem> choices, SelectionContext context) {
        Random random = new Random();
        return choices.get(random.nextInt(choices.size()));
    }
}
