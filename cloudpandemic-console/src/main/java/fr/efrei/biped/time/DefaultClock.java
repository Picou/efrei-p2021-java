package fr.efrei.biped.time;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

public class DefaultClock extends Clock {
    @Override
    public ZoneId getZone() {
        return ZoneId.systemDefault();
    }

    @Override
    public Clock withZone(ZoneId zone) {
        return Clock.system(zone);
    }

    @Override
    public Instant instant() {
        return Instant.now();
    }
}
