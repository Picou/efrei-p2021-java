package fr.efrei.biped.userInterface;

import fr.efrei.biped.entities.InhabitantState;
import fr.efrei.biped.simulation.DetailedSimulation;
import fr.efrei.paumier.shared.traits.AgeCategory;
import fr.efrei.paumier.shared.traits.Interests;

import javax.swing.*;
import java.awt.*;

public class DisplayPanel extends JPanel {
    private final DetailedSimulation simulation;

    private final JLabel totalAliveLabel;
    private final JLabel totalInfectedLabel;
    private final JLabel totalQuarantinedLabel;
    private final JLabel youngAliveLabel;
    private final JLabel youngInfectedLabel;
    private final JLabel youngQuarantinedLabel;
    private final JLabel adultAliveLabel;
    private final JLabel adultInfectedLabel;
    private final JLabel adultQuarantinedLabel;
    private final JLabel seniorAliveLabel;
    private final JLabel seniorInfectedLabel;
    private final JLabel seniorQuarantinedLabel;
    private final JLabel sportAliveLabel;
    private final JLabel sportInfectedLabel;
    private final JLabel sportQuarantinedLabel;
    private final JLabel cultureAliveLabel;
    private final JLabel cultureInfectedLabel;
    private final JLabel cultureQuarantinedLabel;
    private final JLabel nightlifeAliveLabel;
    private final JLabel nightlifeInfectedLabel;
    private final JLabel nightlifeQuarantinedLabel;

    public DisplayPanel(DetailedSimulation sim) {
        super(new GridBagLayout());
        this.simulation = sim;

        this.totalAliveLabel = new JLabel();
        this.totalInfectedLabel = new JLabel();
        this.totalQuarantinedLabel = new JLabel();
        this.youngAliveLabel = new JLabel();
        this.youngInfectedLabel = new JLabel();
        this.youngQuarantinedLabel = new JLabel();
        this.adultAliveLabel = new JLabel();
        this.adultInfectedLabel = new JLabel();
        this.adultQuarantinedLabel = new JLabel();
        this.seniorAliveLabel = new JLabel();
        this.seniorInfectedLabel = new JLabel();
        this.seniorQuarantinedLabel = new JLabel();
        this.sportAliveLabel = new JLabel();
        this.sportInfectedLabel = new JLabel();
        this.sportQuarantinedLabel = new JLabel();
        this.cultureAliveLabel = new JLabel();
        this.cultureInfectedLabel = new JLabel();
        this.cultureQuarantinedLabel = new JLabel();
        this.nightlifeAliveLabel = new JLabel();
        this.nightlifeInfectedLabel = new JLabel();
        this.nightlifeQuarantinedLabel = new JLabel();
        setComponents();

        // Items we add are added in each grid cell in order,
        // first in first row (from left to right), then to next row, and so on
    }

    private void setComponents() {
        GridBagConstraints gbc = new GridBagConstraints();

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 1;
        gbc.gridy = 0;
        add(new JLabel("En vie"), gbc);
        gbc.gridx = 2;
        gbc.gridy = 0;
        add(new JLabel("Contaminé"), gbc);
        gbc.gridx = 3;
        gbc.gridy = 0;
        add(new JLabel("En quarantaine") , gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 1;
        add(new JLabel("Total"), gbc);
        gbc.gridx = 1;
        gbc.gridy = 1;
        add(totalAliveLabel, gbc);
        gbc.gridx = 2;
        gbc.gridy = 1;
        add(totalInfectedLabel, gbc);
        gbc.gridx = 3;
        gbc.gridy = 1;
        add(totalQuarantinedLabel, gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 2;
        add(new JLabel("YOUNG"), gbc);
        gbc.gridx = 1;
        gbc.gridy = 2;
        add(youngAliveLabel, gbc);
        gbc.gridx = 2;
        gbc.gridy = 2;
        add(youngInfectedLabel, gbc);
        gbc.gridx = 3;
        gbc.gridy = 2;
        add(youngQuarantinedLabel, gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 3;
        add(new JLabel("ADULT"), gbc);
        gbc.gridx = 1;
        gbc.gridy = 3;
        add(adultAliveLabel, gbc);
        gbc.gridx = 2;
        gbc.gridy = 3;
        add(adultInfectedLabel, gbc);
        gbc.gridx = 3;
        gbc.gridy = 3;
        add(adultQuarantinedLabel, gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 4;
        add(new JLabel("SENIOR"), gbc);
        gbc.gridx = 1;
        gbc.gridy = 4;
        add(seniorAliveLabel, gbc);
        gbc.gridx = 2;
        gbc.gridy = 4;
        add(seniorInfectedLabel, gbc);
        gbc.gridx = 3;
        gbc.gridy = 4;
        add(seniorQuarantinedLabel, gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 5;
        add(new JLabel("SPORTS"), gbc);
        gbc.gridx = 1;
        gbc.gridy = 5;
        add(sportAliveLabel, gbc);
        gbc.gridx = 2;
        gbc.gridy = 5;
        add(sportInfectedLabel, gbc);
        gbc.gridx = 3;
        gbc.gridy = 5;
        add(sportQuarantinedLabel, gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 6;
        add(new JLabel("CULTURE"), gbc);
        gbc.gridx = 1;
        gbc.gridy = 6;
        add(cultureAliveLabel, gbc);
        gbc.gridx = 2;
        gbc.gridy = 6;
        add(cultureInfectedLabel, gbc);
        gbc.gridx = 3;
        gbc.gridy = 6;
        add(cultureQuarantinedLabel, gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 7;
        add(new JLabel("NIGHT_LIFE"), gbc);
        gbc.gridx = 1;
        gbc.gridy = 7;
        add(nightlifeAliveLabel, gbc);
        gbc.gridx = 2;
        gbc.gridy = 7;
        add(nightlifeInfectedLabel, gbc);
        gbc.gridx = 3;
        gbc.gridy = 7;
        add(nightlifeQuarantinedLabel, gbc);
    }

    public void refresh() {
        //TODO mettre la valeur de chacune des données dans les labels
        this.totalAliveLabel.setText(Integer.toString(this.simulation.getTotalNbInhabitant(InhabitantState.ALIVE)));
        this.totalInfectedLabel.setText(Integer.toString(this.simulation.getTotalNbInhabitant(InhabitantState.INFECTED)));
        this.totalQuarantinedLabel.setText(Integer.toString(this.simulation.getTotalNbInhabitant(InhabitantState.QUARANTINED)));
        this.youngAliveLabel.setText(Long.toString(this.simulation.getNbInhabitant(AgeCategory.YOUNG, InhabitantState.ALIVE)));
        this.youngInfectedLabel.setText(Long.toString(this.simulation.getNbInhabitant(AgeCategory.YOUNG, InhabitantState.INFECTED)));
        this.youngQuarantinedLabel.setText(Long.toString(this.simulation.getNbInhabitant(AgeCategory.YOUNG, InhabitantState.QUARANTINED)));
        this.adultAliveLabel.setText(Long.toString(this.simulation.getNbInhabitant(AgeCategory.ADULT, InhabitantState.ALIVE)));
        this.adultInfectedLabel.setText(Long.toString(this.simulation.getNbInhabitant(AgeCategory.ADULT, InhabitantState.INFECTED)));
        this.adultQuarantinedLabel.setText(Long.toString(this.simulation.getNbInhabitant(AgeCategory.ADULT, InhabitantState.QUARANTINED)));
        this.seniorAliveLabel.setText(Long.toString(this.simulation.getNbInhabitant(AgeCategory.SENIOR, InhabitantState.ALIVE)));
        this.seniorInfectedLabel.setText(Long.toString(this.simulation.getNbInhabitant(AgeCategory.SENIOR, InhabitantState.INFECTED)));
        this.seniorQuarantinedLabel.setText(Long.toString(this.simulation.getNbInhabitant(AgeCategory.SENIOR, InhabitantState.QUARANTINED)));
        this.sportAliveLabel.setText(Long.toString(this.simulation.getNbInhabitant(Interests.SPORT, InhabitantState.ALIVE)));
        this.sportInfectedLabel.setText(Long.toString(this.simulation.getNbInhabitant(Interests.SPORT, InhabitantState.INFECTED)));
        this.sportQuarantinedLabel.setText(Long.toString(this.simulation.getNbInhabitant(Interests.SPORT, InhabitantState.QUARANTINED)));
        this.cultureAliveLabel.setText(Long.toString(this.simulation.getNbInhabitant(Interests.CULTURE, InhabitantState.ALIVE)));
        this.cultureInfectedLabel.setText(Long.toString(this.simulation.getNbInhabitant(Interests.CULTURE, InhabitantState.INFECTED)));
        this.cultureQuarantinedLabel.setText(Long.toString(this.simulation.getNbInhabitant(Interests.CULTURE, InhabitantState.QUARANTINED)));
        this.nightlifeAliveLabel.setText(Long.toString(this.simulation.getNbInhabitant(Interests.NIGHT_LIFE, InhabitantState.ALIVE)));
        this.nightlifeInfectedLabel.setText(Long.toString(this.simulation.getNbInhabitant(Interests.NIGHT_LIFE, InhabitantState.INFECTED)));
        this.nightlifeQuarantinedLabel.setText(Long.toString(this.simulation.getNbInhabitant(Interests.NIGHT_LIFE, InhabitantState.QUARANTINED)));
    }
}
