package fr.efrei.biped.userInterface;

import fr.efrei.biped.simulation.DetailedSimulation;
import fr.efrei.paumier.shared.orders.OrderType;

import java.util.Scanner;

/**
 * Interface utilisateur en console
 */
public class Console implements Runnable {

    private final DetailedSimulation simulation;

    public Console(DetailedSimulation sim) {
        this.simulation = sim;
    }


    private DetailedSimulation getSimulation() {
        return simulation;
    }


    @Override
    public void run() {
        OutputThread threadOutput = new OutputThread();
        threadOutput.start();

        Scanner sc = new Scanner(System.in);
        String input;
        label:
        while (true) {
            input = sc.next();
            switch (input) {
                case "q":
                    //Arreter le thread output
                    threadOutput.killThread();
                    break label;
                case "d":
                    Console.this.getSimulation().executeOrder(OrderType.BUILD_SCREENING_CENTER);
                    break;
                case "i":
                    Console.this.getSimulation().executeOrder(OrderType.INCREASE_TAXES);
                    break;
                case "m":
                    Console.this.getSimulation().executeOrder(OrderType.RESEARCH_MEDICINE);
                    break;
                case "v":
                    Console.this.getSimulation().executeOrder(OrderType.RESEARCH_VACCINE);
                    break;
            }
        }
    }

    class OutputThread extends Thread {
        private boolean running;

        @Override
        public void run() {
            running = true;

            System.out.println("La simulation demarre\n\n");


            do {
                simulation.update();
                display();
                try {
                    synchronized (this) {
                        wait(3000);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Appuyez sur \"q\" puis entree pour quitter");
                System.out.println("Appuyez sur \"d\" puis entree pour acheter un centre de depistage");
                System.out.println("Appuyez sur \"m\" puis entree pour lancer la recherche d'un medicament");
                System.out.println("Appuyez sur \"v\" puis entree pour lancer la recherche d'un vaccin");
                System.out.println("Appuyez sur \"i\" puis entree pour augmenter les impots\n\n");
            }
            while (this.running);


        }

        private void display() {
            System.out.println("A l\'instant : " + simulation.getCurrentInstant().toString() +
                    "\nHabitants en vie (Jeune/Adulte/Âgé) : " + simulation.displayLivingPopulationByAge() +
                    "\nHabitants contaminés (Jeune/Adulte/Âgé) : " + simulation.displayInfectedPopulationByAge() +
                    "\nHabitants en quarantaine (Jeune/Adulte/Âgé) : " + simulation.displayQuarantinedPopulationByAge() +
                    "\nHabitants en vie (Sport/Culture/Soirée) : " + simulation.displayLivingPopulationByInterests() +
                    "\nHabitants contaminés (Sport/Culture/Soirée) : " + simulation.displayInfectedPopulationByInterests() +
                    "\nHabitants en quarantaine (Sport/Culture/Soirée) : " + simulation.displayQuarantinedPopulationByInterests() +
                    "\nImpots de la ville : " + simulation.getMoney() + "\n");

        }

        void killThread() {
            this.running = false;
        }
    }
}
