package fr.efrei.biped.userInterface;

import fr.efrei.biped.simulation.DetailedSimulation;
import fr.efrei.paumier.shared.orders.OrderType;

import javax.swing.*;

public class ActionPanel extends JPanel {
    private final DetailedSimulation simulation;

    private final JLabel moneyLabel;
    private OrderType selectedOrder;

    public ActionPanel(DetailedSimulation simulation) {
        this.simulation = simulation;

        this.moneyLabel = new JLabel();
        refreshMoney("XXXXX");
        // We use XXXXX to reserve space for enough numbers, when auto-size will be used.
        // This is crude but efficient enough for the kind of application we write.

        add(moneyLabel);
        add(createOrderComboBox());
        add(createPurchasingButton());
    }

    private JButton createPurchasingButton() {
        JButton purchaseButton = new JButton();
        purchaseButton.setText("Purchase");
        purchaseButton.addActionListener(event -> purchaseSelectedOrder());
        return purchaseButton;
    }

    private JComboBox<OrderType> createOrderComboBox() {
        JComboBox<OrderType> orderComboBox = new JComboBox<>();
        orderComboBox.addItemListener(event -> setOrder(((OrderType)event.getItem())));

        for(OrderType order : OrderType.values()) {
            orderComboBox.addItem(order);
        }
        // Note: when adding the first item, it is automatically selected
        // which calls the item listener (if it was assigned before)

        return orderComboBox;
    }

    private void setOrder(OrderType order) {
        this.selectedOrder = order;
    }

    private void purchaseSelectedOrder() {
        this.simulation.executeOrder(selectedOrder);
    }

    public void refresh() {
        refreshMoney(Long.toString(simulation.getMoney()));
    }

    private void refreshMoney(String moneyAmount) {
        this.moneyLabel.setText("Money: " + moneyAmount);
    }
}

