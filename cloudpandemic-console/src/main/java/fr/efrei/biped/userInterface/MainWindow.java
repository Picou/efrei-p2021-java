package fr.efrei.biped.userInterface;

import fr.efrei.biped.simulation.DetailedSimulation;

import javax.swing.*;
import java.awt.*;

public class MainWindow extends JFrame {
    private DisplayPanel displayPanel;
    private ActionPanel actionPanel;

    public  MainWindow(DetailedSimulation sim) {
        this.displayPanel = new DisplayPanel(sim);
        this.displayPanel.setBorder(BorderFactory.createEmptyBorder(5, 15, 5 ,15));
        this.actionPanel = new ActionPanel(sim);

        JPanel mainPanel = createMainPanel();
        this.setContentPane(mainPanel);
        this.setLocationRelativeTo(null);
        this.pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private JPanel createMainPanel() {
        // Border layouts is often a good fit for a content pane, as the top level organization of the window
        var panel = new JPanel(new BorderLayout());
        panel.add(displayPanel, BorderLayout.CENTER);
        panel.add(actionPanel,BorderLayout.PAGE_END);
        return panel;
    }

    public void refresh() {
        this.displayPanel.refresh();
        this.actionPanel.refresh();
    }
}
