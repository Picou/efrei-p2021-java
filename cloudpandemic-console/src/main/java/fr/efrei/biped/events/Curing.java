package fr.efrei.biped.events;

import fr.efrei.biped.entities.City;
import fr.efrei.biped.entities.Inhabitant;

import java.time.Duration;

/**
 * Evènement de guérison d'un habitant mis en quarantaine
 */
class Curing extends EventWithSetDuration {
    private final Inhabitant inhabitant;
    private final City city;

    public Curing(Duration duration, Inhabitant inhabitant, City city) {
        super(duration);
        this.inhabitant = inhabitant;
        this.city = city;
    }

    @Override
    public void trigger() {
        if(inhabitant.getIsAlive()) {
            inhabitant.cure();
        }

        reducePanic();
    }

    private void reducePanic() {
        if(this.city.getPanicLevel() - 2 > 0)
            this.city.setPanicLevel(this.city.getPanicLevel() - 2);
        else
            this.city.setPanicLevel(0);
    }
}
