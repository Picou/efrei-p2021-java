package fr.efrei.biped.events;

import fr.efrei.biped.entities.City;

import java.time.Duration;

public class ResearchVaccine extends EventWithSetDuration {
    private final City city;

    public ResearchVaccine(Duration duration, City city) {
        super(duration);
        this.city = city;
    }
    @Override
    public void trigger() {
        this.city.setLifespanCoeff((float) (this.city.getLifespanCoeff() * 1.25));

    }
}
