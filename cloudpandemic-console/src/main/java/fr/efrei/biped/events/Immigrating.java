package fr.efrei.biped.events;

import fr.efrei.biped.entities.City;
import fr.efrei.biped.entities.Inhabitant;

import java.time.Duration;

public class Immigrating extends EventWithSetDuration {
    private final City city;
    private final boolean isInfected;

    public Immigrating(City city, Duration duration, boolean isInfected) {
        super(duration);
        this.city = city;
        this.isInfected = isInfected;
    }

    @Override
    public void trigger() {
        synchronized (this.city) {
            int inhabitantId = this.city.newInhabitantId();
            Inhabitant inhabitant = new Inhabitant(inhabitantId,
                    this.city.getTraitsProvider().getInterests(inhabitantId),
                    this.city.getTraitsProvider().getAgeCategory(inhabitantId));

            if (this.isInfected) {
                inhabitant.infect();

            }

            this.city.inhabitantImmigrating(inhabitant);
        }
    }
}
