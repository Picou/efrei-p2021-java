package fr.efrei.biped.events;

import fr.efrei.biped.entities.City;

import java.time.Duration;

public class BuildingClinic extends EventWithSetDuration {
    private final City city;

    public BuildingClinic(Duration duration, City city) {
        super(duration);
        this.city = city;
    }

    @Override
    public void trigger() {
        this.city.setScreeningCoeff(this.city.getScreeningCoeff() + 1);
    }
}
