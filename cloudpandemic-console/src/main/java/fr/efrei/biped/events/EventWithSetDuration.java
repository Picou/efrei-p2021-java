package fr.efrei.biped.events;

import fr.efrei.paumier.shared.events.Event;

import java.time.Duration;

abstract class EventWithSetDuration implements Event {
    private final Duration duration;

    EventWithSetDuration(Duration duration) {
        this.duration = duration;
    }

    @Override
    public Duration getDuration() {
        return this.duration;
    }
}
