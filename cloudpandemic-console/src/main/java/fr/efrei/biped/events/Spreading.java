package fr.efrei.biped.events;

import fr.efrei.biped.entities.City;
import fr.efrei.biped.entities.Inhabitant;
import fr.efrei.paumier.shared.selection.SelectionContext;

import java.time.Duration;

/**
 * Evènement de contamination d'un infecté vers un habitant sain
 */
class Spreading extends EventWithSetDuration {
    private final Inhabitant infected;
    private final City city;

    Spreading(Duration duration, City city, Inhabitant infected) {
        super(duration);
        this.city = city;
        this.infected = infected;
    }

    @Override
    public void trigger() {
        //sélection parmi les habitants et propagation de l'infection
        if(!infected.getIsQuarantined() &&  infected.getIsInfected() && infected.getIsAlive()) {
            if(city.getInhabitantsNormal().size() > 0) {
                Inhabitant newInfected = city.getSelector()
                        .selectAmong(city.getInhabitantsToInfect(/*infected.getAgeRange(), */infected.getHobbies()), SelectionContext.Spreading);
                infected.contaminate(newInfected);
                this.city.registerKilling(newInfected);
                registerSpreading(newInfected);
            }
            registerSpreading(infected);

            this.city.setPanicLevel(this.city.getPanicLevel() + 2);
            if(this.city.getPanicLevel() > this.city.getLivingPopulation()) {
                this.city.inhabitantEmigrating();
            }
        }
    }

    private void registerSpreading(Inhabitant infected) {
        this.city.getEngine().register(new Spreading(Duration.ofSeconds(5), city, infected));
    }
}
