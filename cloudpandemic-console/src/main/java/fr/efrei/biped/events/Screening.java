package fr.efrei.biped.events;

import fr.efrei.biped.entities.City;
import fr.efrei.biped.entities.Inhabitant;
import fr.efrei.paumier.shared.selection.SelectionContext;

import java.time.Duration;
import java.util.ArrayList;

/**
 * Evènement de dépistage
 */
public class Screening extends EventWithSetDuration {
    private final City city;

    public Screening(Duration duration, City city) {
        super(duration);
        this.city = city;
    }

    @Override
    public void trigger() {
        //si tout le monde est en quarantaine, le dépistage est inutile
        if(city.getLivingPopulation() != city.getQuarantinedPopulation()) {
            for (int i = 0; i < city.getScreeningCoeff(); i++) {
                ArrayList<Inhabitant> undetectedInhabitants = new ArrayList<>(city.getInhabitantsAlive());
                undetectedInhabitants.removeAll(city.getQuarantinedInhabitants());
                Inhabitant screened = city.getSelector().selectAmong(undetectedInhabitants, SelectionContext.Screening);
                if (screened.getIsInfected()) {
                    putInQuarantine(screened);
                }

            }
        }
        //ajoute un nouvel évènement screening
        city.getEngine().register(new Screening(Duration.ofMillis(200), city));
    }

    private void putInQuarantine(Inhabitant infected) {
        infected.setIsQuarantined(true);
        city.getEngine()
                .register(new Curing(Duration.ofMillis((long)city.getCuringTime() * 1000), infected, city));
    }
}
