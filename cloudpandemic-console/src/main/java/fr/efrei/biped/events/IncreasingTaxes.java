package fr.efrei.biped.events;

import fr.efrei.biped.entities.City;

import java.time.Duration;

public class IncreasingTaxes extends EventWithSetDuration {
    private final City city;

    public IncreasingTaxes(Duration duration, City city) {
        super(duration);
        this.city = city;
    }

    @Override
    public void trigger() {
        this.city.setTaxesCoeff(this.city.getTaxesCoeff() + 1);
        this.city.setPanicLevel(this.city.getPanicLevel() + 5);
        if(this.city.getPanicLevel() > this.city.getLivingPopulation()) {
            this.city.inhabitantEmigrating();
        }
    }
}
