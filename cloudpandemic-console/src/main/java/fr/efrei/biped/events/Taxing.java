package fr.efrei.biped.events;

import fr.efrei.biped.entities.City;
import fr.efrei.biped.entities.Inhabitant;

import java.time.Duration;
import java.util.ArrayList;

public class Taxing extends EventWithSetDuration {
    private final City city;

    public Taxing(Duration duration, City city) {
        super(duration);
        this.city = city;
    }

    @Override
    public void trigger() {
        ArrayList<Inhabitant> livingInhabitants = new ArrayList<>(city.getInhabitantsAlive());
        city.setFunds(city.getFunds() + (long)(livingInhabitants.stream()
                .mapToDouble(Inhabitant::getBaseTax)
                .sum()
                * city.getTaxesCoeff()));

        city.getEngine().register(new Taxing(Duration.ofSeconds(20), city));
    }
}
