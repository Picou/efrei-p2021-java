package fr.efrei.biped.events;

import fr.efrei.biped.entities.City;
import fr.efrei.biped.entities.Inhabitant;
import fr.efrei.paumier.shared.selection.SelectionContext;

import java.time.Duration;

/**
 * Evènement d'infection initiale
 */
public class Infect extends EventWithSetDuration {
    private final City city;
    private final Inhabitant inhabitant;

    public Infect(Duration duration, City city) {
        super(duration);
        this.city = city;
        this.inhabitant = null;
    }

    @Override
    public void trigger() {
        //sélection parmi les habitants et démarrage l'infection
        Inhabitant inhabitant;
        if (this.inhabitant != null) {
            inhabitant = this.inhabitant;
        } else {
            inhabitant = city.getSelector()
                    .selectAmong(city.getInhabitantsNormal(), SelectionContext.InitialInfection);
        }
        inhabitant.infect();
        city.registerKilling(inhabitant);
        city.getEngine().register(new Spreading(Duration.ofSeconds(5), city, inhabitant));

        this.city.setPanicLevel(this.city.getPanicLevel() + 2);
        if(this.city.getPanicLevel() > this.city.getLivingPopulation()) {
            this.city.inhabitantEmigrating();
        }
    }

}
