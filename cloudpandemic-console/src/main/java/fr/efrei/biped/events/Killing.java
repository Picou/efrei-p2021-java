package fr.efrei.biped.events;

import fr.efrei.biped.entities.City;
import fr.efrei.biped.entities.Inhabitant;

import java.time.Duration;

/**
 * Evènement pour tuer un habitant infecté
 */
public class Killing extends EventWithSetDuration {
    private final Inhabitant inhabitant;
    private final City city;

    public Killing(Duration duration, Inhabitant inhabitant, City city) {
        super(duration);
        this.inhabitant = inhabitant;
        this.city = city;
    }

    @Override
    public void trigger() {
        if(inhabitant.getIsInfected()) {
            inhabitant.die();
        }

        this.city.setPanicLevel(this.city.getPanicLevel() + 5);
        if(this.city.getPanicLevel() > this.city.getLivingPopulation()) {
            this.city.inhabitantEmigrating();
        }

    }
}
