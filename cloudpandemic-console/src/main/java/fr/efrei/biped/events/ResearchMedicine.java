package fr.efrei.biped.events;

import fr.efrei.biped.entities.City;

import java.time.Duration;

public class ResearchMedicine extends EventWithSetDuration {
    private final City city;

    public ResearchMedicine(Duration duration, City city) {
        super(duration);
        this.city = city;
    }
    @Override
    public void trigger() {
        this.city.setCuringTime((float) (this.city.getCuringTime() * 0.8));

    }
}
