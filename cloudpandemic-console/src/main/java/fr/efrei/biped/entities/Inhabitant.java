package fr.efrei.biped.entities;


import fr.efrei.paumier.shared.traits.AgeCategory;
import fr.efrei.paumier.shared.traits.Interests;

import java.util.List;

public class Inhabitant {

    private final int id;

    private boolean isInfected;
    private boolean isQuarantined;
    private boolean isDead;
    private final List<Interests> hobbies;

    private final AgeCategory ageRange;

    public Inhabitant(int id, List<Interests> hobbies, AgeCategory ageRange){
        this.id = id;
        this.isInfected = false;
        this.isQuarantined = false;
        this.isDead = false;
        this.hobbies = hobbies;
        this.ageRange = ageRange;

    }

    public void infect(){
        this.isInfected = true;
    }

    public void contaminate(Inhabitant inhabitant){
        if(this.getIsInfected() && !this.getIsQuarantined() && this.getIsAlive()){
            inhabitant.setInfected();
        }
    }

    public void die(){    // les habitant ne peuvent mourir qu'à cause de l'infection
        if(this.getIsInfected()){
            this.setIsQuarantined(false);
            this.setDead();
        }
    }

    public void cure(){ // les habitants ne peuvent guérir qu'en quarantaine
        if(this.getIsInfected() && this.getIsQuarantined() && this.getIsAlive()){
            isInfected = false;
            isQuarantined = false;
        }
    }



    /*public String getNameInhabitant() {
        return nameInhabitant;
    }*/
    public boolean getIsInfected() {
        if(!this.isDead)
            return isInfected;
        return false;
    }

    private void setInfected() {
        isInfected = true;
    }
    public boolean getIsQuarantined() {
        if(!this.isDead)
            return isQuarantined;
        return false;
    }

    public void setIsQuarantined(boolean isQuarantined) {
        this.isQuarantined = isQuarantined;
    }
    public boolean getIsAlive() {
        return !isDead;
    }

    private void setDead() {
        isDead = true;
    }
    public double getBaseTax() {
        switch (this.ageRange) {
            case ADULT:
                return 1;
            case YOUNG:
                return 0.5;
            case SENIOR:
                return 0.75;
        }
        return 0;
    }

    public List<Interests> getHobbies() {
        return this.hobbies;
    }

    public AgeCategory getAgeRange() {
        return this.ageRange;
    }

    public int getId() {
        return this.id;
    }
}
