package fr.efrei.biped.entities;

import fr.efrei.biped.events.*;
import fr.efrei.paumier.shared.engine.GameEngine;
import fr.efrei.paumier.shared.networking.EmigrationMessage;
import fr.efrei.paumier.shared.selection.SelectionContext;
import fr.efrei.paumier.shared.selection.Selector;
import fr.efrei.paumier.shared.traits.AgeCategory;
import fr.efrei.paumier.shared.traits.Interests;
import fr.efrei.paumier.shared.traits.TraitsProvider;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class City {

    private final GameEngine engine;
    private final Selector selector;

    private final ArrayList<Inhabitant> inhabitants;
    private int countInhabitants;
    private final TraitsProvider traitsProvider;

    private long funds;
    private float curingTime;
    private int screeningCoeff;
    private float taxesCoeff;
    private float lifespanCoeff;
    private int panicLevel;

    private final ObjectOutputStream objectOutputStream;


    public City(int nbInhabitants, GameEngine engine, TraitsProvider traitsProvider, Selector selector, int initialPanic, ObjectOutputStream objectOutputStream) {
        this.countInhabitants = 0;
        this.traitsProvider = traitsProvider;
        this.inhabitants = new ArrayList<>();
        for(int i = 0; i < nbInhabitants; i++) {
            int inhabitantId = newInhabitantId();
            inhabitants.add(new Inhabitant(inhabitantId,
                    this.traitsProvider.getInterests(inhabitantId),
                    this.traitsProvider.getAgeCategory(inhabitantId)));
        }
        this.engine = engine;
        this.selector = selector;
        this.screeningCoeff = 1;
        this.taxesCoeff = 1;
        this.curingTime = 5;
        this.lifespanCoeff = 1;
        this.funds = 0;
        this.panicLevel = initialPanic;
        /*this.socket = socket;*/
        this.objectOutputStream = objectOutputStream;
    }

    public int newInhabitantId() {
        int inhabitantId = this.countInhabitants;
        this.countInhabitants++;
        return inhabitantId;
    }

    public void registerKilling(Inhabitant newInfected) {
        switch (newInfected.getAgeRange()) {
            case SENIOR:
                this.engine.register(
                        new Killing(Duration.ofMillis(5 * (long) (this.lifespanCoeff * 1000)), newInfected, this));
                break;
            case YOUNG:
                this.engine.register(
                        new Killing(Duration.ofMillis((long) (7500 * this.lifespanCoeff)), newInfected, this));
                break;
            case ADULT:
                this.engine.register(
                        new Killing(Duration.ofMillis(15 * (long) (this.lifespanCoeff * 1000)), newInfected, this));
                break;
        }
    }

    public ArrayList<Inhabitant> getInhabitantsToInfect(/*AgeCategory ageCategory, */List<Interests> interests) {
        /*ArrayList<Inhabitant> truc =  (ArrayList<Inhabitant>) this.inhabitants.stream()
                .filter(inhabitant -> !inhabitant.getIsInfected())
                .filter(Inhabitant::getIsNotDead)
                .filter(inhabitant -> !inhabitant.getIsQuarantined())
                .flatMap(o -> Collections.nCopies(2, o).stream() //FIXME
                        .filter(inhabitant -> inhabitant.getAgeRange() == ageCategory))
                .flatMap(o -> Collections.nCopies(2, o).stream() //FIXME
                        .filter(inhabitant -> inhabitant.getAgeRange() == ageCategory))
                .sorted(Comparator.comparing(Inhabitant::getId))
                .collect(Collectors.toList());
        return truc;*/
        ArrayList<Inhabitant> toInfect =  getInhabitantsNormal();
        toInfect.addAll(getInhabitantsNormal().stream()
                .filter(inhabitant -> !Collections.disjoint(inhabitant.getHobbies(), interests))
                .collect(Collectors.toList()));
        return  (ArrayList<Inhabitant>) toInfect.stream()
                .sorted(Comparator.comparing(Inhabitant::getId))
                .collect(Collectors.toList());
    }

    public boolean canBuyUpgrade(int price) {
        return this.funds >= price;
    }

    public void buyUpgrade(int price) {
        this.funds -= price;
    }

    public ArrayList<Inhabitant> getQuarantinedInhabitants() {
        return (ArrayList<Inhabitant>) this.inhabitants.stream()
                .filter(Inhabitant::getIsQuarantined)
                .collect(Collectors.toList());
    }

    public ArrayList<Inhabitant> getInhabitantsAlive() {
        return (ArrayList<Inhabitant>) this.inhabitants.stream()
                .filter(Inhabitant::getIsAlive)
                .collect(Collectors.toList());
    }

    private ArrayList<Inhabitant> getInfectedInhabitants() {
        return (ArrayList<Inhabitant>) this.inhabitants.stream()
                .filter(Inhabitant::getIsInfected)
                .collect(Collectors.toList());
    }

    public ArrayList<Inhabitant> getInhabitantsNormal() {
        return (ArrayList<Inhabitant>) this.inhabitants.stream()
                .filter(inhabitant -> !inhabitant.getIsInfected())
                .filter(Inhabitant::getIsAlive)
                .filter(inhabitant -> !inhabitant.getIsQuarantined())
                .collect(Collectors.toList());
    }
    public int getQuarantinedPopulation(){
        return (int)this.inhabitants.stream()
                .filter(Inhabitant::getIsQuarantined)
                //.map(e -> 1)
                //.reduce(0, Integer::sum);
                .count();


    }

    public int getInfectedPopulation() {
        return (int)this.inhabitants.stream()
                .filter(Inhabitant::getIsInfected)
                .count();

    }

    public int getOriginalPopulation() {
        return this.inhabitants.size();
    }

    public int getLivingPopulation() { //tous les habitants sauf les morts
        return (int)this.inhabitants.stream()
                .filter(Inhabitant::getIsAlive)
                .count();
    }

    @Override
    public String toString() {
        return "Habitants à l'origine : " + getOriginalPopulation() +
                "\nHabitants en vie : " + getLivingPopulation() +
                "\nHabitants contaminés : " + getInfectedPopulation() +
                "\nHabitants en quarantaine : " + getQuarantinedPopulation();
    }

    public long getFunds() {
        return this.funds;
    }

    public long getLivingPopulationByAge(AgeCategory ageCategory) {
        return this.inhabitants.stream()
                .filter(inhabitant -> inhabitant.getAgeRange() == ageCategory)
                .filter(Inhabitant::getIsAlive)
                .count();
    }

    private ArrayList<Inhabitant> getAliveInhabitantsByAge(AgeCategory ageCategory) {
        return (ArrayList<Inhabitant>) this.inhabitants.stream()
                .filter(inhabitant -> inhabitant.getAgeRange() == ageCategory)
                .filter(Inhabitant::getIsAlive)
                .collect(Collectors.toList());
    }

    private ArrayList<Inhabitant> getAliveInhabitantsByInterests(Interests interests) {
        return (ArrayList<Inhabitant>) this.inhabitants.stream()
                .filter(inhabitant -> inhabitant.getHobbies().contains(interests))
                .filter(Inhabitant::getIsAlive)
                .collect(Collectors.toList());
    }

    public long getLivingPopulationByInterests(Interests interest) {
        return this.inhabitants.stream()
                .filter(inhabitant -> inhabitant.getHobbies().contains(interest))
                .filter(Inhabitant::getIsAlive)
                .count();
    }

    public long getInfectedPopulationByAge(AgeCategory ageCategory) {
        return this.inhabitants.stream()
                .filter(inhabitant -> inhabitant.getAgeRange() == ageCategory)
                .filter(Inhabitant::getIsInfected)
                .filter(Inhabitant::getIsAlive)
                .count();
    }

    public long getInfectedPopulationByInterests(Interests interest) {
        return this.inhabitants.stream()
                .filter(inhabitant -> inhabitant.getHobbies().contains(interest))
                .filter(Inhabitant::getIsInfected)
                .filter(Inhabitant::getIsAlive)
                .count();
    }

    public long getQuarantinedPopulationByAge(AgeCategory ageCategory) {
        return this.inhabitants.stream()
                .filter(inhabitant -> inhabitant.getAgeRange() == ageCategory)
                .filter(Inhabitant::getIsQuarantined)
                .filter(Inhabitant::getIsAlive)
                .count();
    }

    public long getQuarantinedPopulationByInterests(Interests interest) {
        return this.inhabitants.stream()
                .filter(inhabitant -> inhabitant.getHobbies().contains(interest))
                .filter(Inhabitant::getIsQuarantined)
                .filter(Inhabitant::getIsAlive)
                .count();
    }

    public int getNbInhabitants(InhabitantState inhabitantState) {
        switch (inhabitantState) {
            case ALIVE:
                return getLivingPopulation();
            case INFECTED:
                return getInfectedPopulation();
            case QUARANTINED:
                return getQuarantinedPopulation();
            default:
                return 0;
        }
    }

    public List<Inhabitant> getAlivePopulationByState(InhabitantState inhabitantState) {
        switch (inhabitantState) {
            case ALIVE:
                return getInhabitantsAlive();
            case INFECTED:
                return getInfectedInhabitants();
            case QUARANTINED:
                return getQuarantinedInhabitants();
            default:
                return null;
        }
    }

    public long getNbInhabitants(AgeCategory ageCategory, InhabitantState inhabitantState) {
        return this.inhabitants.stream()
                .filter(getAlivePopulationByState(inhabitantState)::contains)
                .filter(getAliveInhabitantsByAge(ageCategory)::contains)
                .count();
    }

    public long getNbInhabitants(Interests interests, InhabitantState inhabitantState) {
        return this.inhabitants.stream()
                .filter(getAlivePopulationByState(inhabitantState)::contains)
                .filter(getAliveInhabitantsByInterests(interests)::contains)
                .count();
    }

    public int getScreeningCoeff() {
        return screeningCoeff;
    }

    public float getTaxesCoeff() {
        return taxesCoeff;
    }

    public GameEngine getEngine() {
        return engine;
    }

    public Selector getSelector() {
        return selector;
    }

    public float getCuringTime() {
        return curingTime;
    }

    public float getLifespanCoeff() {
        return lifespanCoeff;
    }

    public void setFunds(long funds) {
        this.funds = funds;
    }

    public void setTaxesCoeff(float taxesCoeff) {
        this.taxesCoeff = taxesCoeff;
    }

    public void setScreeningCoeff(int screeningCoeff) {
        this.screeningCoeff = screeningCoeff;
    }

    public void setCuringTime(float curingTime) {
        this.curingTime = curingTime;
    }

    public void setLifespanCoeff(float lifespanCoeff) {
        this.lifespanCoeff = lifespanCoeff;
    }

    public int getPanicLevel() {
        return this.panicLevel;
    }

    public TraitsProvider getTraitsProvider() {
        return traitsProvider;
    }

    public void setPanicLevel(int panicLevel) {
        this.panicLevel = panicLevel;
    }

    public void inhabitantImmigrating(Inhabitant immigrant) {
        this.inhabitants.add(immigrant);
    }

    public synchronized void inhabitantEmigrating() {
        if(this.objectOutputStream != null) {
            ArrayList<Inhabitant> inhabitants = new ArrayList<>(getInhabitantsAlive());
            inhabitants.removeAll(getQuarantinedInhabitants());

            Inhabitant emigrant = this.selector.selectAmong(inhabitants, SelectionContext.Emigration);

            try {
                objectOutputStream.writeObject(new EmigrationMessage(emigrant.getIsInfected()));
            } catch (IOException e) {
                e.printStackTrace();
            }

            this.inhabitants.remove(emigrant);

            this.panicLevel -= 5;

            if(this.panicLevel > getLivingPopulation()) {
                inhabitantEmigrating();
            }
        }
    }
}
