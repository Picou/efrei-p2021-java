package fr.efrei.biped.entities;

public enum InhabitantState {
    ALIVE,
    INFECTED,
    QUARANTINED
}
