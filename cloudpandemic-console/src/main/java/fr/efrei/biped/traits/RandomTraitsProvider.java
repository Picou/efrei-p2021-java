package fr.efrei.biped.traits;

import fr.efrei.paumier.shared.traits.AgeCategory;
import fr.efrei.paumier.shared.traits.Interests;
import fr.efrei.paumier.shared.traits.TraitsProvider;

import java.util.*;

public class RandomTraitsProvider implements TraitsProvider {
    private final HashMap<Integer, AgeCategory> ageCategoryPerId = new HashMap<>();
    private final HashMap<Integer, List<Interests>> interestsPerId = new HashMap<>();

    @Override
    public AgeCategory getAgeCategory(int inhabitantId) {
        if(this.ageCategoryPerId.get(inhabitantId) != null) {
            return this.ageCategoryPerId.get(inhabitantId);
        }
        setInhabitantTraits(inhabitantId);
        return this.ageCategoryPerId.get(inhabitantId);
    }

    @Override
    public List<Interests> getInterests(int inhabitantId) {
        if(this.interestsPerId.get(inhabitantId) != null) {
            return this.interestsPerId.get(inhabitantId);
        }
        setInhabitantTraits(inhabitantId);
        return this.interestsPerId.get(inhabitantId);
    }

    private void setInhabitantTraits(int inhabitantId) {
        // variables locales plutôt que plusieurs appel avec la même valeur
        Random random = new Random();
        List<AgeCategory> ageCategoryValues = List.of(AgeCategory.values());
        List<Interests> interestsValues = List.of(Interests.values());

        this.ageCategoryPerId.put(inhabitantId, ageCategoryValues.get(random.nextInt(ageCategoryValues.size())));


        int nbInterests = random.nextInt(interestsValues.size()) + 1;
        ArrayList<Interests> inhabitantInterests = new ArrayList<>();
        for(int i = 0; i < nbInterests; i++) {
            inhabitantInterests.add(interestsValues.get(random.nextInt(interestsValues.size())));
        }
        this.interestsPerId.put(inhabitantId, inhabitantInterests);
    }
}
