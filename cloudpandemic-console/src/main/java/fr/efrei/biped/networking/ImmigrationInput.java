package fr.efrei.biped.networking;

import fr.efrei.biped.entities.City;
import fr.efrei.biped.events.Immigrating;
import fr.efrei.paumier.shared.engine.GameEngine;
import fr.efrei.paumier.shared.networking.ImmigrationMessage;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.time.Duration;

public class ImmigrationInput implements Runnable {
    private final Thread thread;
    private final GameEngine engine;
    private final ObjectInputStream objectInputStream;
    private final City city;

    private volatile boolean shouldStop;

    public ImmigrationInput(GameEngine engine, City city, ObjectInputStream objectInputStream) {
        this.engine = engine;
        this.objectInputStream = objectInputStream;
        this.city = city;

        this.shouldStop = false;
        this.thread = new Thread(this);
    }

    public void run() {
        while(true) {
            if (this.shouldStop) {
                return;
            }

            ImmigrationMessage message;
            try {
                message = (ImmigrationMessage) this.objectInputStream.readObject();
                this.engine.register(new Immigrating(this.city, Duration.ZERO, message.isInfected()));
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }

        }
    }

    public void start() {
        this.thread.start();
    }

    public boolean isRunning() {
        return this.thread.isAlive();
    }

    public void stop() {
        this.shouldStop = true;
        this.thread.interrupt();
    }

    public void join() throws InterruptedException {
        this.thread.join();
    }
}
