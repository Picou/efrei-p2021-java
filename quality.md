# Qualité

* Nommage variables: 1
* Usage adéquat des fonctionnalités du langage: 1
* Simplicité des fonctions: 0.6
* Simplicité des classes: 1
* Pas de dupplication de code: 0.8
* Séparation de responsabilités: 0.9
* Encapsulation: 1
* Intégration dans une application: 1
